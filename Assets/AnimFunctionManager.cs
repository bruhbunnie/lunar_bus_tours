﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimFunctionManager : MonoBehaviour
{
    public List<AudioClip> footSteps;

    public List<AudioClip> metalFootSteps;

    public AudioSource audioSource;


    public void PlayFootStep()
    {
        int listCount;
        int randomNum;
        List<AudioClip> clipsList;

        if (CameraControl._instance.isOnBuss)
        {
            listCount = metalFootSteps.Count;
            clipsList = metalFootSteps;
            audioSource.volume = .2f;
        }
        else
        {
            listCount = footSteps.Count;
            clipsList = footSteps;
            audioSource.volume = .8f;
        }

        randomNum = Random.Range(0, listCount - 1);

        audioSource.clip = clipsList[randomNum];

        audioSource.Play();
    }
}
