﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager _instance;

    public SeaofTAudio seaofT;

    public AudioSource nodeSource;

    public PlayerSFX playerSFX;

    public AudioSource sfxSource00;

    public AudioSource sfxSource01;

    public void Awake()
    {
        if (_instance == null)
            _instance = this;
        if (_instance != this)
            Destroy(this);
    }

    public AudioClip Recording(int constIndex, int trackIndex)
    {
        AudioClip clipCapture;

        clipCapture = seaofT.audioRecordings[constIndex].recordings[trackIndex];

        return clipCapture;
    }


    public void PlayerClick()
    {
        if (!sfxSource00.isPlaying)
        {
            sfxSource00.clip = playerSFX.playerClick;
            sfxSource00.volume = 0.75f;

            float randomPitch = Random.Range(1.0f, 1.2f);
            sfxSource00.pitch = randomPitch;

            sfxSource00.Play();
        }
        else
        {
            sfxSource01.clip = playerSFX.playerClick;
            sfxSource01.volume = 0.75f;

            float randomPitch = Random.Range(1.0f, 1.2f);
            sfxSource01.pitch = randomPitch;

            sfxSource01.Play();
        }

    }

    public void StarComplete()
    {
        if (!sfxSource00.isPlaying)
        {
            sfxSource00.clip = playerSFX.starsComplete;
            sfxSource00.volume = 0.5f;
            sfxSource00.pitch = 1f;
            sfxSource00.Play();
        }
        else
        {
            sfxSource01.clip = playerSFX.starsComplete;
            sfxSource01.volume = 0.5f;
            sfxSource01.pitch = 1f;
            sfxSource01.Play();
        }
    }

    public void StarSelect()
    {

        if (!sfxSource00.isPlaying)
        {
            sfxSource00.clip = playerSFX.starSelect;
            sfxSource00.volume = 0.5f;

            float randomPitch = Random.Range(1.0f, 1.2f);
            sfxSource00.pitch = randomPitch;

            sfxSource00.Play();
        }
        else
        {
            sfxSource01.clip = playerSFX.starSelect;
            sfxSource01.volume = 0.5f;

            float randomPitch = Random.Range(1.0f, 1.2f);
            sfxSource01.pitch = randomPitch;

            sfxSource01.Play();
        }
    }

    public void UISlide()
    {
        if (!sfxSource00.isPlaying)
        {
            sfxSource00.clip = playerSFX.uiSlide;
            sfxSource00.volume = 0.2f;
            sfxSource00.pitch = 1f;
            sfxSource00.Play();
        }
        else
        {
            sfxSource01.clip = playerSFX.uiSlide;
            sfxSource01.volume = 0.2f;
            sfxSource01.pitch = 1f;
            sfxSource01.Play();
        }
    }
}
