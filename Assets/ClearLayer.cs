﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ClearLayer : MonoBehaviour, IuiActivate
{
    public Image buttonImage;

    public Color highlightColour;
    public Color defaultColour;

    public Collider buttonCollider;

    void Start()
    {

    }

    public void SetToActive()
    {

    }

    public void OnActivated()
    {
        BadgeManager._instance.ClearLayerImage();
    }

    public void OnHighlight()
    {
        if (buttonImage.color == defaultColour)
        {
            buttonImage.color = highlightColour;
        }
    }

    public bool IsLit()
    {
        if (buttonImage.color != defaultColour)
            return true;
        else
            return false;
    }


    public void OnDefault()
    {
       buttonImage.color = defaultColour;

    }
}
