﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCollection : MonoBehaviour
{
    public List<Collider> uiColliders = new List<Collider>();

    public bool isActive = false;

    void Awake()
    {
        CollectColliders();
    }

    void CollectColliders()
    {
        uiColliders.AddRange(GetComponentsInChildren<Collider>()); 
    }

    public void ColliderSwitch(bool isColliding)
    {
        foreach (Collider collider in uiColliders)
        {
            collider.enabled = isColliding;
        }
    }
}
