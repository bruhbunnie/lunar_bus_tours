﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ConsoleBlinker : MonoBehaviour
{
    public static ConsoleBlinker _instance;

    public Image buttonImage;

    public TMP_Text textLabel;

    public Color blinkColour;
    public Color defaultColour;

    public TutorialPageBtns tutBtns;

    public GameObject toConParticle;

    public GameObject awayConParticle;

    public GameObject blinkParticle;

    Coroutine blinkRoutine;

    private void Awake()
    {
        _instance = this;
    }
    void Start()
    {
        //defaultColour = tutBtns.defaultColour;

        if (tutBtns.isBlinking)
            blinkRoutine = StartCoroutine("BlinkRoutine");

        awayConParticle.SetActive(false);
    }

    public void TurnOffExitParticle()
    {
        awayConParticle.SetActive(false);
    }
    public void StopBlinkRoutine()
    {
        if (blinkRoutine != null)
            StopCoroutine(blinkRoutine);

        buttonImage.color = defaultColour;
        textLabel.color = defaultColour;

        toConParticle.SetActive(false);
        blinkParticle.SetActive(false);

        awayConParticle.SetActive(true);
    }

    public IEnumerator BlinkRoutine()
    {
        while (tutBtns.isBlinking)
        {
            yield return null;
            /*
            buttonImage.color = blinkColour;
            textLabel.color = blinkColour;
            yield return new WaitForSeconds(.2f);

            buttonImage.color = tutBtns.defaultColour;
            textLabel.color = tutBtns.defaultColour;
            yield return new WaitForSeconds(.5f);
            */
        }

        StopBlinkRoutine();
    }
}
