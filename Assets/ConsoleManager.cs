﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleManager : MonoBehaviour
{
    public static ConsoleManager _instance;

    public List<menuName> menus = new List<menuName>();

    public List<CollisionCollection> colCollections = new List<CollisionCollection>();

    public List<GameObject> sideImages = new List<GameObject>();

    public List<GameObject> locationObjects = new List<GameObject>();

    public GameObject moon;

    public GameObject moonRender;

    public GameObject locationRender;

    public List<GameObject> locationArrows = new List<GameObject>();

    public List<UIMover> uiMovers = new List<UIMover>();

    public GameObject HomeBtn;

    public bool sidePanelsAreShowing = false;

    void Awake()
    {
        if (_instance == null)
            _instance = this;
        if (_instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        ActivateMenu("welcome");
        TurnOffSideImages();
    }

    public void ShowSidePanels()
    {
        if (sidePanelsAreShowing)
        {
            return;
        }

        foreach (UIMover mover in uiMovers)
        {
            mover.mode = UIMoverMode.MoveTo;
            AudioManager._instance.UISlide();
            mover.Move();
            sidePanelsAreShowing = true;
        }
    }

    public void HideSidePanels()
    {
        if (!sidePanelsAreShowing)
        {
            return;
        }

        foreach (UIMover mover in uiMovers)
        {
            mover.mode = UIMoverMode.MoveFrom;
            AudioManager._instance.UISlide();
            mover.Move();
            sidePanelsAreShowing = false;
        }
    }

    public void ImagePicker(string objectName, int index)
    {
        TurnOffSideImages();

        switch (objectName)
        {
            case "image":
                for (int i = 0; i < sideImages.Count; i++)
                {
                    if (index == sideImages[i].GetComponent<ImageIdentity>().imageIndex)
                        sideImages[i].SetActive(true);
                }
                break;

            case "level":
                for (int i = 0; i < locationArrows.Count; i++)
                {
                    if (index == locationArrows[i].GetComponent<ImageIdentity>().imageIndex)
                        locationArrows[i].SetActive(true);

                    if (index == locationObjects[i].GetComponent<ImageIdentity>().imageIndex)
                        locationObjects[i].SetActive(true);

                }

                moon.SetActive(true);
                moonRender.SetActive(true);
                locationRender.SetActive(true);
                break;





            default:
                Debug.Log("you spelled it WRONG!");
                break;
        } 
    }

    public void TurnOffSideImages()
    {
        foreach (GameObject image in sideImages)
        {
            image.SetActive(false);
        }

        foreach (GameObject arrows in locationArrows)
        {
            arrows.SetActive(false);
        }

        foreach (GameObject locations in locationObjects)
        {
            locations.SetActive(false);
        }

        moonRender.SetActive(false);
        locationRender.SetActive(false);
        moon.SetActive(false);
    }

    public void TurnOffMenus()
    {
        foreach (var menu in menus)
        {
            menu.gameObject.SetActive(false);
        }

        foreach (var col in colCollections)
        {
            col.ColliderSwitch(false);
        }
    }

    public void ActivateMenu(string menuName)
    {
        TurnOffMenus();
        HomeBtn.SetActive(true);

        for (int i = 0; i < menus.Count; i++)
        {
            if (menus[i].menu == menuName)
            {
                menus[i].gameObject.SetActive(true);
                colCollections[i].ColliderSwitch(true);
                break;
            }
        }

        switch (menuName)
        {
            case "home":
                HomeBtn.SetActive(false);
                HideSidePanels();
                break;

            case "level":
                ImagePicker("level", LevelSelectManager._instance.levelIndex);
                ShowSidePanels();
                break;

            case "welcome":
                TutorialPagesManager._instance.PageChange(0);
                break;

        }
    }
}
