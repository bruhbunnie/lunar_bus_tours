﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagManager : MonoBehaviour
{
    public static FlagManager _instance;

    public GameObject playerFlag;

    public Vector3 playerPos;

    public List<GameObject> flagSprites = new List<GameObject>();

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        if (_instance != this)
            Destroy(gameObject);
    }


    void Start()
    {
        playerFlag.SetActive(false);

        //StartCoroutine(DropFlagRoutine());
    }

    public void PlacePlayerFlag()
    {
        playerFlag.SetActive(true);
        playerFlag.transform.parent = null;
        playerFlag.transform.position = PlayerManager._instance.gameObject.transform.localPosition;
    }

    public IEnumerator DropFlagRoutine()
    {
        while (!playerFlag.activeSelf)
        {
            playerPos = PlayerManager._instance.gameObject.transform.position;

            if (Input.GetKeyDown(KeyCode.F))
                PlacePlayerFlag();

            yield return null;
        }
    }
}
