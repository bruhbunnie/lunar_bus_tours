﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectManager : MonoBehaviour
{
    public static LevelSelectManager _instance;

    public int levelIndex;

    void Awake()
    {
        if (_instance == null)
            _instance = this;
        if (_instance != this)
            Destroy(this);
    }
}
