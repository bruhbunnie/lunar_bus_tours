﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelect_i : MonoBehaviour, IuiActivate
{
    public TMP_Text textLabel;

    public Color highlightColour;
    public Color defaultColour;

    public Collider buttonCollider;

    public int locationIndex;

    public bool isLocked;

    void Start()
    {
        textLabel.color = defaultColour;
        buttonCollider = GetComponent<Collider>();
    }

    public void SetToActive()
    {

    }

    public void OnActivated()
    {
        if (isLocked)
            return;

        LevelSelectManager._instance.levelIndex = locationIndex;
    }


    public void ChangeLocation()
    {
        switch (locationIndex)
        {
            case 0:
                ConsoleManager._instance.ImagePicker("level", 0);
                break;
            case 1:
                ConsoleManager._instance.ImagePicker("level", 1);
                break;
            case 2:
                ConsoleManager._instance.ImagePicker("level", 2);

                break;
        }
    }

    public void OnHighlight()
    {
        if (textLabel.color == defaultColour)
        {
            textLabel.color = highlightColour;
            ChangeLocation();
        }
    }

    public bool IsLit()
    {
        if (textLabel.color == highlightColour)
            return true;
        else
            return false;
    }


    public void OnDefault()
    {
        textLabel.color = defaultColour;
    }
}


