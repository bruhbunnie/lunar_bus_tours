﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaskTrigger : MonoBehaviour
{
    public Mask bckgMask;

    public Mask bckgRenderMask;

    public Image bckgImage;

    public static MaskTrigger _instance;

    private void Awake()
    {
        _instance = this;
    }

    public void MaskActive()
    {
        if(bckgImage.sprite != null)
        {
            bckgMask.enabled = true;
            bckgRenderMask.enabled = true;
        }
        else
{
            bckgMask.enabled = false;
            bckgRenderMask.enabled = false;
        }
    }

    private void Start()
    {
        MaskActive();
    }
}
