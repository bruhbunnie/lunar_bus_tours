﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour, IuiActivate
{
    public Image buttonImage;

    public TMP_Text textLabel;

    public Color highlightColour;
    public Color defaultColour;

    public Collider buttonCollider;

    public string menuName;

    void Start()
    {
        buttonCollider = GetComponent<Collider>();

        if (buttonImage != null)
            buttonImage.color = defaultColour;
    }

    public void SetToActive()
    {

    }

    public void OnActivated()
    {
        ConsoleManager._instance.ActivateMenu(menuName);
        OnDefault();
    }

    public void OnHighlight()
    {
        if (buttonImage != null)
        {
            if (buttonImage.color == defaultColour)
            {
                if (buttonImage != null)
                    buttonImage.color = highlightColour;

                textLabel.color = highlightColour;
            }
        }
        else
        {
            if (textLabel.color == defaultColour)
                textLabel.color = highlightColour;
        }

    }

    public bool IsLit()
    {
        if (buttonImage != null)
        {
            if (buttonImage.color == highlightColour)
                return true;
            else
                return false;
        }
        else
        {
            if (textLabel.color == highlightColour)
                return true;
            else
                return false;
        }

    }


    public void OnDefault()
    {
        if (buttonImage != null)
            buttonImage.color = defaultColour;

        textLabel.color = defaultColour;
    }
}

