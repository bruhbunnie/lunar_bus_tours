﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSFX : MonoBehaviour
{
    public AudioClip playerClick;

    public AudioClip starSelect;

    public AudioClip starsComplete;

    public AudioClip uiSlide;
}
