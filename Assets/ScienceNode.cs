﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScienceNode : MonoBehaviour
{
    public GameObject playerObject;

    public List<ObjectMover> uiObjectMovers;

    public Collision playerDetect;

    Coroutine playerClose;

    public bool isActive;

    // animation time
    public float moveTime = 1f;

    // delay before iTween animation begins
    public float delay = 0f;

    // ease in-out
    public iTween.EaseType easeType = iTween.EaseType.easeOutExpo;

    private void Start()
    {
        UIStartPosition();
    }

    public void UIStartPosition()
    {
        foreach (ObjectMover mover in uiObjectMovers)
        {
            mover.mode = GraphicMoverMode.MoveToHide;
            mover.moveTime = 0f;
            mover.easeType = iTween.EaseType.linear;
            mover.delay = 0f;

            mover.Move();
        }
    }

    public void UIHidePosition()
    {
        foreach (ObjectMover mover in uiObjectMovers)
        {
            mover.mode = GraphicMoverMode.MoveToHide;
            mover.moveTime = moveTime;
            mover.easeType = easeType;
            mover.delay = delay;

            mover.Move();
        }
    }

    public void UIShowPosition()
    {
        foreach (ObjectMover mover in uiObjectMovers)
        {
            mover.mode = GraphicMoverMode.MoveToShow;
            mover.moveTime = moveTime;
            mover.easeType = easeType;
            mover.delay = delay;

            mover.Move();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        playerObject = other.gameObject;

        isActive = true;

        CameraControl._instance.FirstPersonView();

        UIShowPosition();

        //if (playerClose != null)
           // StopCoroutine(playerClose);

        //playerClose = StartCoroutine(PlayerClose());
    }

    private IEnumerator PlayerClose()
    {
        CameraControl._instance.FirstPersonView();

        while (true)
        {
            UIShowPosition();
            yield return null;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        UIHidePosition();

        playerObject = null;

        isActive = false;

        CameraControl._instance.ThirdPersonView();

        //StopCoroutine(playerClose);
    }
}
