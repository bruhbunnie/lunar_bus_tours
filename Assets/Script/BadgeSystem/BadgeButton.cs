﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BadgeButton : MonoBehaviour, IuiActivate
{
    public Image highlight;

    public Image badge;

    public Color inactiveColour;
    public Color highlightColour;
    public Color defaultColour;

    public bool isActive;

    public Collider buttonCollider;

    void Start()
    {
        highlight.color = inactiveColour;
        buttonCollider = GetComponent<Collider>();

        badge = transform.GetChild(0).GetComponent<Image>();
    }

    public void SetToActive()
    {
        BadgeDisplayManager._instance.SetBadgeToActive();

        isActive = true;
    }

    public void OnActivated()
    {
        if (isActive)
            return;

        BadgeDisplayManager._instance.SetBadgeToActive();

        isActive = true;
        BadgeManager._instance.ChangeLayerImage(badge.sprite);

        if (BadgeRenderManager._instance != null)
            BadgeRenderManager._instance.MakeSprite();
    }

    public void OnHighlight()
    {
        if (badge.color == defaultColour)
        {
            highlight.color = highlightColour;
        }
    }

    public bool IsLit()
    {
        if (highlight.color == highlightColour)
            return true;
        else
            return false;
    }


    public void OnDefault()
    {
        highlight.color = inactiveColour;

    }
}
