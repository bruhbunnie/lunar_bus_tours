﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BadgeDisplayManager : MonoBehaviour
{
    public static BadgeDisplayManager _instance;

    List<BadgeButton> badgeButtons = new List<BadgeButton>();

    private void Awake()
    {
        _instance = this;

        CollectBadgePositions();
        
    }

    public void CollectBadgePositions()
    {

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf)
                badgeButtons.Add(transform.GetChild(i).GetComponent<BadgeButton>());
        }
    }

    public void ChangeBadgeImage(int index, Sprite sprite)
    {
        badgeButtons[index].badge.sprite = sprite;
    }

    public void TurnOffBadgeHighlight()
    {
        foreach (BadgeButton badgeButton in badgeButtons)
        {
            if (badgeButton.badge.color == badgeButton.highlightColour)
                badgeButton.OnDefault();
        }
    }

    public void SetBadgeToActive()
    {
        foreach (BadgeButton badgeButton in badgeButtons)
        {
            badgeButton.isActive = false;
            badgeButton.OnDefault();
        }
    }

    public void UpdateBadgeImages(List<Sprite> sprites)
    {
        for (int i = 0; i < badgeButtons.Count; i++)
        {
            badgeButtons[i].badge.color = badgeButtons[i].defaultColour;

            if (i > (sprites.Count - 1))
                badgeButtons[i].badge.color = badgeButtons[i].inactiveColour;
            else
                ChangeBadgeImage(i, sprites[i]);
        }
    }
}
