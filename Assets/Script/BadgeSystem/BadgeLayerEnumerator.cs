﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BadgeLayer
{
    EMPTY,
    Background,
    Border,
    Detail_01,
    Detail_02,
}
