﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BadgeManager : MonoBehaviour
{
    public static BadgeManager _instance;

    public BadgeLayer badgeLayer;

    public List<Image> layerImages = new List<Image>();

    public List<LayerButton> layerButtons = new List<LayerButton>();

    public TMP_Text badgeText;
    public TMP_InputField initialsText;
    public TMP_Text randomizerText;

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        ChangeLayer(BadgeLayer.EMPTY);
    }

    public void UpdateBadgeText()
    {
        badgeText.text = initialsText.text + " " + randomizerText.text;

        if (BadgeRenderManager._instance != null)
            BadgeRenderManager._instance.MakeSprite();
    }


    public void ChangeLayer(BadgeLayer badgeLayer)
    {
        this.badgeLayer = badgeLayer;

        switch (badgeLayer)
        {
            case BadgeLayer.EMPTY:
                Debug.Log("Nothin to badge about");
                break;
            case BadgeLayer.Background:
                BadgeNColourManager._instance.badgeSorter.BreakUpLists(BadgeGroups.Background);
                break;
            case BadgeLayer.Border:
                BadgeNColourManager._instance.badgeSorter.BreakUpLists(BadgeGroups.Border);
                break;
            case BadgeLayer.Detail_01:
                BadgeNColourManager._instance.badgeSorter.BreakUpLists(BadgeGroups.Detail);
                break;
            case BadgeLayer.Detail_02:
                BadgeNColourManager._instance.badgeSorter.BreakUpLists(BadgeGroups.Detail);
                break;
        }
    }

    public void ClearLayerImage()
    {
        switch (badgeLayer)
        {
            case BadgeLayer.EMPTY:
                Debug.Log("Nothin to badge about");
                break;
            case BadgeLayer.Border:
                layerImages[0].sprite = null;
                layerImages[0].color = new Color(0, 0, 0, 0);
                break;
            case BadgeLayer.Background:
                layerImages[1].sprite = null;
                layerImages[1].color = new Color(0, 0, 0, 0);
                MaskTrigger._instance.MaskActive();
                break;
            case BadgeLayer.Detail_01:
                layerImages[2].color = new Color(0, 0, 0, 0);
                layerImages[2].sprite = null;
                break;
            case BadgeLayer.Detail_02:
                layerImages[3].sprite = null;
                layerImages[3].color = new Color(0, 0, 0, 0);
                break;
        }
    }

    public void ChangeLayerImage(Sprite sprite)
    {
        switch (badgeLayer)
        {
            case BadgeLayer.EMPTY:
                Debug.Log("Nothin to badge about");
                break;
            case BadgeLayer.Border:
                layerImages[0].sprite = sprite;
                AlphaCheck(0);
                break;
            case BadgeLayer.Background:
                layerImages[1].sprite = sprite;
                AlphaCheck(1);
                MaskTrigger._instance.MaskActive();
                break;
            case BadgeLayer.Detail_01:
                layerImages[2].sprite = sprite;
                AlphaCheck(2);
                break;
            case BadgeLayer.Detail_02:
                layerImages[3].sprite = sprite;
                AlphaCheck(3);
                break;

        }
    }

    public void AlphaCheck(int layerNumber)
    {
        if (layerImages[layerNumber].color.a == 0)
        {
            switch (layerNumber)
            {
                case 0:
                    layerImages[layerNumber].color = new Color(.9f, .9f, .9f, 1);
                    break;
                case 1:
                    layerImages[layerNumber].color = new Color(.8f, .8f, .8f, 1);
                    break;
                case 2:
                    layerImages[layerNumber].color = new Color(.7f, .7f, .7f, 1);
                    break;
                case 3:
                    layerImages[layerNumber].color = new Color(.6f, .6f, .6f, 1);
                    break;
            }
        }
    }

    public void ChangeLayerColour(Color colour)
    {
        switch (badgeLayer)
        {
            case BadgeLayer.EMPTY:
                Debug.Log("Nothin to badge about");
                break;
            case BadgeLayer.Border:
                if (!LayerAlphaCheck(0))
                    return;
                layerImages[0].color = colour;
                break;
            case BadgeLayer.Background:
                if (!LayerAlphaCheck(1))
                    return;
                layerImages[1].color = colour;
                break;
            case BadgeLayer.Detail_01:
                if (!LayerAlphaCheck(2))
                    return;
                layerImages[2].color = colour;
                break;
            case BadgeLayer.Detail_02:
                if (!LayerAlphaCheck(3))
                    return;
                layerImages[3].color = colour;
                break;
        }
    }

    public bool LayerAlphaCheck(int layerInt)
    {
        if (layerImages[layerInt].color.a == 0)
            return false;
        else
            return true;
    }

    public void TurnOffHighLight()
    {
        foreach (LayerButton layerButton in layerButtons)
        {
            layerButton.OnDefault();
        }
    }

    public void SetToActive(BadgeLayer layer)
    {
        foreach (LayerButton layerButton in layerButtons)
        {
            if (layer != layerButton.layer)
            {
                layerButton.isActive = false;
                layerButton.OnDefault();
            }

        }
    }
}
