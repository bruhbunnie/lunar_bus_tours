﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BadgeRenderManager : MonoBehaviour
{
    public static BadgeRenderManager _instance;

    public List<Image> cameraImages;

    public List<Image> badgeImages;

    public TMP_Text cameraText;

    public TMP_Text badgeText;

    public Camera badgeCamera;
    public int height = 512;
    public int width = 512;
    int depth = 24;

    //public GameObject playerBadge;

    private void Awake()
    {
        _instance = this;
    }

    public void MakeSprite()
    {
        cameraText.text = badgeText.text;

        for (int i = 0; i < badgeImages.Count; i++)
        {
            cameraImages[i].sprite = badgeImages[i].sprite;
            cameraImages[i].color = badgeImages[i].color;
        }

        RenderTexture renderTexture = new RenderTexture(width, height, depth);
        Rect rect = new Rect(0, 0, width, height);
        Texture2D texture = new Texture2D(width, height, TextureFormat.RGBA32, false);

        badgeCamera.targetTexture = renderTexture;
        badgeCamera.Render();

        RenderTexture currentRenderTexture = RenderTexture.active;
        RenderTexture.active = renderTexture;
        texture.ReadPixels(rect, 0, 0);
        texture.Apply();

        badgeCamera.targetTexture = null;
        RenderTexture.active = currentRenderTexture;
        Destroy(renderTexture);

        Sprite sprite = Sprite.Create(texture, rect, new Vector2(.5f, .5f));

        PlayerManager._instance.playerBadge.GetComponent<SpriteRenderer>().sprite = sprite;

        foreach (GameObject image in FlagManager._instance.flagSprites)
        {
            image.GetComponent<SpriteRenderer>().sprite = sprite;
        }
    }
}
