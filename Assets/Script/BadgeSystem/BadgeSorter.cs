﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Sprites
{
    public List<Sprite> sprites;
}

[System.Serializable]
public class SpritesList
{
    public List<Sprites> lists;
}

public enum BadgeGroups
{
    Background,
    Border,
    Detail
}

public class BadgeSorter : MonoBehaviour
{
    public BadgeImageCollection badgeCollection;

    public SpritesList ListOfSpritesLists;

    public int listAmount;

    public int positionAmount = 20;

    private void Awake()
    {
        //badgeCollection = GetComponent<BadgeCollection>();
    }

    private void Start()
    {
        //BreakUpLists(BadgeGroups.Detail);
    }

    public void BreakUpLists(BadgeGroups badgeGroups)
    {
        ListOfSpritesLists.lists.Clear();

        switch (badgeGroups)
        {
            case BadgeGroups.Background:
                BreakUpLoops(badgeCollection.badgeBackgrounds);
                break;
            case BadgeGroups.Border:
                BreakUpLoops(badgeCollection.badgeBorders);
                break;
            case BadgeGroups.Detail:
                BreakUpLoops(badgeCollection.badgeDetails);
                
                break;
        }
    }

    public void BreakUpLoops(List<Sprite> spriteList)
    {
        for (int i = 0; i < ListMath(spriteList); i++)
        {
            ListOfSpritesLists.lists.Add(new Sprites());
        }

        int spriteIndex = 0;

        for (int i = 0; i < ListOfSpritesLists.lists.Count; i++)
        {
            

            ListOfSpritesLists.lists[i].sprites = new List<Sprite>();

            ListOfSpritesLists.lists[i].sprites.Clear();

            for (int j = 0; j < ListinListMath(i + 1, spriteList.Count); j++)
            {
                ListOfSpritesLists.lists[i].sprites.Add(spriteList[spriteIndex]);

                spriteIndex++;
            }

        }
        
    }

    int ListMath(List<Sprite> sprites)
    {
        listAmount = Mathf.CeilToInt(sprites.Count / positionAmount) + 1;
        return listAmount;
    }

    int ListinListMath(int loops, int imageCount)
    {
        if ((positionAmount * loops) < imageCount)
            return positionAmount;
        else
            return (positionAmount - ((positionAmount * loops) - imageCount));
    }
}
