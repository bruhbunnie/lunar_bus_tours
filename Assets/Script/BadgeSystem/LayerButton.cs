﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LayerButton : MonoBehaviour, IuiActivate
{
    public Image buttonImage;

    public Color inactiveColour;
    public Color highlightColour;
    public Color activeColour;

    public bool isActive;

    public Collider buttonCollider;

    public BadgeLayer layer;

    void Start()
    {
        buttonCollider = GetComponent<Collider>();

        //buttonImage = GetComponent<Image>();

        buttonImage.color = inactiveColour;
    }

    public void SetToActive()
    {
        BadgeManager._instance.SetToActive(layer);

        isActive = true;
        buttonImage.color = activeColour;

    }

    public void OnActivated()
    {
        if (isActive)
            return;

        BadgeManager._instance.SetToActive(layer);

        isActive = true;
        buttonImage.color = activeColour;

        BadgeManager._instance.ChangeLayer(layer);

        PageButtonManager._instance.SetListIndex(BadgeNColourManager._instance.badgeSorter.ListOfSpritesLists.lists.Count);

        BadgeDisplayManager._instance.UpdateBadgeImages(BadgeNColourManager._instance.badgeSorter.ListOfSpritesLists.lists[0].sprites);
    }

    public void OnHighlight()
    {
        BadgeManager._instance.TurnOffHighLight();

        buttonImage.color = highlightColour;

    }

    public bool IsLit()
    {
        if (buttonImage.color == activeColour)
            return true;
        else
            return false;
    }


    public void OnDefault()
    {
        if (isActive)
            return;

        buttonImage.color = inactiveColour;

    }
}
