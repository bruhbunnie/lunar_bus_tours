﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constellation : MonoBehaviour
{
    public string contellationName;

    public Sprite constellationIcon;

    public bool isActive = false;

    public bool isCompleted = false;

    public List<GameObject> starPositions;

    public List<StarPositioner> stars;

    public List<GameObject> starsObj;

    public SpriteRenderer constLineArt;

    private void Awake()
    {
        StarPositions();
    }

    private void Start()
    {
        if (constLineArt != null)
            constLineArt.color = new Color(1,1,1,0f);
    }

    public void LineArt()
    {
        float lineartColourInc = .75f /stars.Count;

        if (constLineArt == null)
            return;

        constLineArt.color = new Color(1, 1, 1, constLineArt.color.a + lineartColourInc);
    }

    public void HideLineArt()
    {
        if (constLineArt != null)
            constLineArt.color = new Color(1, 1, 1, 0f);
    }

    void StarPositions()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            if(!gameObject.transform.GetChild(i).gameObject.GetComponent<SpriteRenderer>())
                starPositions.Add(gameObject.transform.GetChild(i).gameObject);
        }
    }

    public void StarCollection()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            if(gameObject.transform.GetChild(i).gameObject.tag == "Star")
            {
                stars.Add(gameObject.transform.GetChild(i).GetComponent<StarPositioner>());

                starsObj.Add(gameObject.transform.GetChild(i).gameObject);
            }
        }

        StarPopulate();
    }

    public void StarPopulate()
    {
        for (int i = 0; i < stars.Count; i++)
        {
            stars[i].starPosition = starPositions[i];
            stars[i].name = "Star_" + i.ToString("00") + "_" + gameObject.name;
            stars[i].GetComponent<StarPositioner>().starGrp = this;
        }
    }

    public void GroupCompleted()
    {
        if (!isCompleted)
        {
            for (int i = 0; i < stars.Count; i++)
            {
                stars[i].StarCollected(false);
            }
        }
    }

    public void StarPositionUpdate()
    {
        foreach (StarPositioner star in stars)
        {
            star.PlaceStar();
        }
    }

    public void StarDistanceUpdate()
    {
        foreach (StarPositioner star in stars)
        {
            star.SetStarDistance();
        }
    }

    public void StarDimmer(bool isDimmed)
    {
        foreach (StarPositioner star in stars)
        {
            star.DimVFX(isDimmed);
        }
    }
}
