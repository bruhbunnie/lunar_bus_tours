﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ConstellationCompare : MonoBehaviour
{
    public static ConstellationCompare _instance;

    public List<GameObject> playerStars;

    public List<GameObject> stars;

    public List<bool> isCompleteds;

    public TMP_Text starCollected;

    public TMP_Text starCount;

    private Coroutine compareUpdaterRoutine;

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        //Debug.Log("compare being ran from Start");
        //CompareUpdater(true);
        //BuildCompletedList();

        if (PlayerManager._instance)
        {
            starCollected = PlayerManager._instance.starCollected;
            starCount = PlayerManager._instance.starCount;
        }

    }

    public void BuildCompletedList()
    {
        isCompleteds.Clear();

        for (int i = 0; i < Constellation_Manager._instance.constellationColections.Count; i++)
        {
            bool index = false;
            isCompleteds.Add(index);
        }
    }

    public void CompareUpdater(bool isChecking)
    {
        if (isChecking)
        {
            if (compareUpdaterRoutine != null)
                StopCoroutine(CompareUpdateRoutine());

            compareUpdaterRoutine = StartCoroutine(CompareUpdateRoutine());
        }
        else
        {
            StopCoroutine(CompareUpdateRoutine());
        }
    }

    public void PlaceFlagCheck()
    {
        if (isCompleteds.Count == Constellation_Manager._instance.constellationColections.Count)
        {
            FlagManager._instance.StartCoroutine(FlagManager._instance.DropFlagRoutine());
            UIOverlayManager._instance.FlagButtonUI(true);
        }
            
    }

    private IEnumerator CompareUpdateRoutine()
    {
        int constellationIndex;

        while (true)
        {
            constellationIndex = ConstellationUI_Controller._instance.constellationIndex;
            starCollected.text = playerStars.Count.ToString();
            starCount.text = stars.Count.ToString();

            ConstellationUI_Controller._instance.isCompletedUI(isCompleteds[constellationIndex]);

            if (playerStars.Count > 0 && playerStars.Count == stars.Count && !isCompleteds[constellationIndex])
            {
                if (CheckMatch())
                {
                    isCompleteds[constellationIndex] = true;
                    Constellation_Manager._instance.constellationRebuild[constellationIndex].isCompleted = true;
                    Constellation_Manager._instance.constellationRebuild[constellationIndex].StarDimmer(true);
                    AudioManager._instance.StarComplete();
                    PlaceFlagCheck();
                    //RunA check on completes to place flag
                }
                else
                {
                    Debug.Log("FailureShake");
                    ClearPlayerList(constellationIndex);
                }
            }

            yield return null;
        }
    }

    public void ClearPlayerList(int constellationIndex)
    {
        //Debug.Log("Constellation index = " + constellationIndex.ToString());

        if (playerStars.Count == 0 || playerStars == null)
        {
            return;
        }

        Constellation_Manager._instance.ConstellationHighlightClear();

        playerStars.Clear();
    }

    private bool CheckMatch()
    {
        int starMatch = 0;

        for (int i = 0; i < stars.Count; i++)
        {
            for (int ii = 0; ii < playerStars.Count; ii++)
            {
                if (stars[i] == playerStars[ii])
                {
                    starMatch++;
                }
            }
        }

        if (starMatch == stars.Count)
            return true;
        else
            return false;
    }
}