﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ConstellationUI_Controller : MonoBehaviour
{
    public static ConstellationUI_Controller _instance;

    public List<Sprite> constellationIcons;

    public List<string> constellationNames;

    public Image iconObject;

    public TMP_Text constellationName;

    public int constellationIndex = 0;

    public GameObject constellationIconObj;

    public GameObject starCounterObj;

    public GameObject completedObj;

    public GameObject noStarsObject;

    public Constellation currentConstellation;

    Coroutine uiContRoutine;

    public void Start()
    {
        _instance = this;
    }

    public void ResetVariables()
    {
        constellationIcons.Clear(); ;

        constellationNames.Clear(); ;

        constellationIndex = 0;

        noStarsObject.SetActive(true);
        constellationIconObj.SetActive(false);
        starCounterObj.SetActive(false);
    }

    public void UIControl(bool controlSwitch)
    {
        if (uiContRoutine != null && controlSwitch == false)
            StopCoroutine(uiContRoutine);
        else
        {
            if (uiContRoutine != null)
                StopCoroutine(uiContRoutine);

            uiContRoutine = StartCoroutine(UIControlRoutine());
        }
    }

    IEnumerator UIControlRoutine()
    {
        while (true)
        {
            if(constellationNames.Count > 1)
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0f)
                {
                    ChangeConstellation(1);
                    UIOverlayManager._instance.ReduceConstellationButtonVisability();
                }

                if (Input.GetKeyDown(KeyCode.Period))
                {
                    ChangeConstellation(1);
                    UIOverlayManager._instance.ReduceConstellationButtonVisability();
                }

                if (Input.GetAxis("Mouse ScrollWheel") < 0f)
                {
                    ChangeConstellation(-1);
                    UIOverlayManager._instance.ReduceConstellationButtonVisability();
                }

                if (Input.GetKeyDown(KeyCode.Comma))
                {
                    ChangeConstellation(-1);
                    UIOverlayManager._instance.ReduceConstellationButtonVisability();
                }
                    
            }

            yield return null;
        }
    }

    public void isCompletedUI(bool isCompleted)
    {
        starCounterObj.SetActive(!isCompleted);
        completedObj.SetActive(isCompleted);
    }

    public void SetConstellation(string constName)
    {
        Debug.Log("ChangeConstellation by Node " + constName);

        if (currentConstellation != null && !currentConstellation.isCompleted)
            currentConstellation.HideLineArt();

        Constellation_Manager._instance.constellationRebuild[constellationIndex].isActive = false;

        for (int i = 0; i < constellationNames.Count; i++)
        {
            if (constName == constellationNames[i])
            {
                Debug.Log("ChangeConstellation by Node " +  i);
                constellationIndex = i;
                break;
            }   
        }

        iconObject.sprite = constellationIcons[constellationIndex];

        constellationName.text = constellationNames[constellationIndex];

        Constellation_Manager._instance.ConstellationDistanceUpdater(constellationIndex);

        ConstellationCompare._instance.stars = Constellation_Manager._instance.constellationRebuild[constellationIndex].starsObj;

        currentConstellation = Constellation_Manager._instance.constellationRebuild[constellationIndex];

        ConstellationCompare._instance.ClearPlayerList(constellationIndex);
    }

    void ChangeConstellation(int inputNumber)
    {
        //if (Constellation_Manager._instance.constellationRebuild.Count > 1)
        //return;
        if (currentConstellation!= null && !currentConstellation.isCompleted)
            currentConstellation.HideLineArt();

        Constellation_Manager._instance.constellationRebuild[constellationIndex].isActive = false;

        if (constellationIndex == (constellationIcons.Count - 1) && inputNumber == 1)
        {
            constellationIndex = 0;
        }
        else if (constellationIndex == 0 && inputNumber == -1)
        {
            constellationIndex = (constellationIcons.Count - 1);
        }
        else
        {
            constellationIndex = constellationIndex + inputNumber;
        }

        iconObject.sprite = constellationIcons[constellationIndex];

        constellationName.text = constellationNames[constellationIndex];

        Constellation_Manager._instance.ConstellationDistanceUpdater(constellationIndex);

        ConstellationCompare._instance.stars = Constellation_Manager._instance.constellationRebuild[constellationIndex].starsObj;

        currentConstellation = Constellation_Manager._instance.constellationRebuild[constellationIndex];

        ConstellationCompare._instance.ClearPlayerList(constellationIndex);
    }

    public void ConstellationAdded()
    {
        //Debug.Log("Consteollation Added");
        if (constellationIcons.Count == 1)
        {
            noStarsObject.SetActive(false);
            constellationIconObj.SetActive(true);
            starCounterObj.SetActive(true);
            ChangeConstellation(0);
        }
        else if (constellationIcons.Count == 2)
        {
            UIControl(true);
            UIOverlayManager._instance.ConstellationButtonUI(true);
        }
    }
}
