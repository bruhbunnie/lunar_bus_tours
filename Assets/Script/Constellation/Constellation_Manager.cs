﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constellation_Manager : MonoBehaviour
{
    public static Constellation_Manager _instance;

    public List<Constellation> constellationColections;

    public List<Constellation> constellationRebuild;

    public int starCount = 0;

    [Range(0.01f, 1f)]
    public float starDistance = .5f;

    [Range(0.01f, 1f)]
    public float starDistMultiplier = 1f;

    private void Awake()
    {
        _instance = this;

        ConstellationCollection();
    }

    private void Start()
    {
        CountAllTheStars();
    }

    void ConstellationCollection()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            if (gameObject.transform.GetChild(i).GetComponent<Constellation>() != null)
                constellationColections.Add(gameObject.transform.GetChild(i).GetComponent<Constellation>());
        }
    }

    void CountAllTheStars()
    {
        for (int i = 0; i < constellationColections.Count; i++)
        {
            starCount += constellationColections[i].starPositions.Count;
        }
    }

    public void ConstellationPositionUpdate()
    {
        foreach (Constellation constellation in constellationColections)
        {
            constellation.StarPositionUpdate();
        }
    }

    public void ConstellationDistanceUpdater(int constellationIndex)
    {
        for (int i = 0; i < constellationRebuild.Count; i++)
        {
            if (i == constellationIndex)
                constellationRebuild[i].isActive = true;
            else
                constellationRebuild[i].isActive = false;

            constellationRebuild[i].StarDistanceUpdate();
        }
    }

    public void ConstellationHighlightClear()
    {
        foreach (var constellation in constellationColections)
        {
            constellation.GroupCompleted();
        }
    }
}
