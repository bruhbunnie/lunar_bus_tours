﻿using System.Collections;
using UnityEngine;

public class HighlightStar : MonoBehaviour
{
    public TrailRenderer trailRender;

    public GameObject trailPivot;

    public GameObject trailObject;

    // point A, our source transform
    public Transform startXform;

    // point B, our target transform
    public Transform endXform;

    // animation time
    public float moveTime = 1f;

    // delay before iTween animation begins
    public float delay = 0f;

    // loop type, if we are animating in a cycle
    public iTween.LoopType loopType = iTween.LoopType.none;

    // ease in-out
    public iTween.EaseType easeType = iTween.EaseType.easeOutExpo;

    private void Start()
    {
        Shakethingy();

        Rotatethingy();
        
    }

    public void Rotatethingy()
    {
        iTween.RotateAdd(trailPivot, iTween.Hash(
            "amount", endXform.transform.eulerAngles,
            "time", moveTime,
            "delay", delay,
            "easetype", easeType,
            "looptype", loopType
            ));
    }

    public void Shakethingy()
    {
        iTween.ShakePosition(trailObject, iTween.Hash(
            "x", 1f,
            "islocal", true,
            "time", 10,
            "delay", delay,
            "easetype", easeType,
            "looptype", loopType
            ));
    }
}