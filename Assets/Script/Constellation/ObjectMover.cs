﻿using System.Collections;
using UnityEngine;

public enum GraphicMoverMode
{
    MoveToHide,
    MoveToShow,
    ScaleTo,
    ScaleFrom,
    MoveFrom,
    RotateTo,
    RotateFrom,
    RotateAdd
}

public class ObjectMover : MonoBehaviour
{
    // what iTween method we are using to transform from point A to point B
    public GraphicMoverMode mode;

    // point A, our source transform
    public Transform startXform;

    // point B, our target transform
    public Transform endXform;

    // animation time
    public float moveTime = 1f;

    // delay before iTween animation begins
    public float delay = 0f;

    // loop type, if we are animating in a cycle
    public iTween.LoopType loopType = iTween.LoopType.none;

    // ease in-out
    public iTween.EaseType easeType = iTween.EaseType.easeOutExpo;

    public bool isAlwaysMoving;

    public bool onStart;

    private void Awake()
    {
        if (endXform == null)
        {
            endXform = new GameObject(gameObject.name + "XformEnd").transform;

            endXform.position = transform.position;
            endXform.eulerAngles = transform.eulerAngles;
            endXform.localScale = transform.localScale;
        }

        if (startXform == null)
        {
            startXform = new GameObject(gameObject.name + "XformStart").transform;

            startXform.position = transform.position;
            startXform.eulerAngles = transform.eulerAngles;
            startXform.localScale = transform.localScale;
        }

        Reset();

        if (onStart)
            Move();
    }

    private void Start()
    {
        //Move();
    }
    // reset the transform to starting values
    public void Reset()
    {
        switch (mode)
        {
            case GraphicMoverMode.MoveToHide:
                if (startXform != null)
                {
                    transform.position = startXform.position;
                }
                break;

            case GraphicMoverMode.MoveToShow:
                if (startXform != null)
                {
                    transform.position = startXform.position;
                }
                break;

            case GraphicMoverMode.MoveFrom:
                if (endXform != null)
                {
                    transform.position = endXform.position;
                }
                break;

            case GraphicMoverMode.RotateTo:
                if (startXform != null)
                {
                    transform.eulerAngles = startXform.eulerAngles;
                }
                break;

            case GraphicMoverMode.RotateFrom:
                if (endXform != null)
                {
                    transform.eulerAngles = endXform.eulerAngles;
                }
                break;

            case GraphicMoverMode.ScaleTo:
                if (startXform != null)
                {
                    transform.localScale = startXform.localScale;
                }
                break;

            case GraphicMoverMode.ScaleFrom:
                if (startXform != null)
                {
                    transform.localScale = endXform.localScale;
                }
                break;
        }
    }

    // scale/rotate/translate the graphic depending on mode
    public void Move()
    {
        if (isAlwaysMoving)
            Reset();

        //Debug.Log("MoveCalled");
        switch (mode)
        {

            case GraphicMoverMode.ScaleTo:
                iTween.ScaleTo(gameObject, iTween.Hash(
                    "scale", endXform.localScale,
                    "time", moveTime,
                    "delay", delay,
                    "easetype", easeType,
                    "looptype", loopType
                ));
                break;

            case GraphicMoverMode.ScaleFrom:
                iTween.ScaleFrom(gameObject, iTween.Hash(
                    "scale", startXform.localScale,
                    "time", moveTime,
                    "delay", delay,
                    "easetype", easeType,
                    "looptype", loopType
                ));
                break;

            case GraphicMoverMode.MoveToHide:
                iTween.MoveTo(gameObject, iTween.Hash(
                    "position", endXform.position,
                    "time", moveTime,
                    "delay", delay,
                    "easetype", easeType,
                    "looptype", loopType
                ));
                break;

            case GraphicMoverMode.MoveToShow:
                iTween.MoveTo(gameObject, iTween.Hash(
                    "position", startXform.position,
                    "time", moveTime,
                    "delay", delay,
                    "easetype", easeType,
                    "looptype", loopType
                ));
                break;

            case GraphicMoverMode.MoveFrom:
                iTween.MoveFrom(gameObject, iTween.Hash(
                    "position", startXform.position,
                    "time", moveTime,
                    "delay", delay,
                    "easetype", easeType,
                    "looptype", loopType
                ));
                break;

            case GraphicMoverMode.RotateAdd:
                iTween.RotateAdd(gameObject, iTween.Hash(
                    "amount", endXform.transform.eulerAngles,
                    "time", moveTime,
                    "delay", delay,
                    "easetype", easeType,
                    "looptype", loopType
                ));
                break;

            case GraphicMoverMode.RotateTo:
                iTween.RotateTo(gameObject, iTween.Hash(
                    "rotation", endXform.transform.eulerAngles,
                    "time", moveTime,
                    "delay", delay,
                    "easetype", easeType,
                    "looptype", loopType
                ));
                break;

            case GraphicMoverMode.RotateFrom:
                iTween.RotateFrom(gameObject, iTween.Hash(
                    "rotation", startXform.transform.eulerAngles,
                    "time", moveTime,
                    "delay", delay,
                    "easetype", easeType,
                    "looptype", loopType
                ));
                break;
            default:
                break;
        }

    }
}