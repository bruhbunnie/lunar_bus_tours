﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public GameObject playerCamera;

    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
        public GameObject starParent;
    }

    public static ObjectPooler _instance;

    public List<Pool> pools;

    private void Awake()
    {
        _instance = this;


    }

    void Start()
    {
        SpawnStars();

        if (PlayerManager._instance)
            playerCamera = PlayerManager._instance.gameObject;
        else
            playerCamera = Constellation_Manager._instance.gameObject;
    }

    void SpawnStars()
    {
        foreach (Pool pool in pools)
        {
            Constellation starGrp = pool.starParent.GetComponent<Constellation>();

            pool.size = starGrp.starPositions.Count;

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);

                StarPositioner starPos = obj.GetComponent<StarPositioner>();

                obj.transform.SetParent(pool.starParent.transform);

                starPos.playerPosition = playerCamera;
            }

            starGrp.StarCollection();

        }
    }
}
