﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarPositioner : MonoBehaviour
{
    public Constellation starGrp;

    public GameObject starPosition;

    public GameObject playerPosition;

    public float distance = .1f;

    public GameObject highlight;

    public SpriteRenderer circle;

    public SpriteRenderer starSprite;

    public GameObject starObj;

    Coroutine placeStarRoutine;

    public Collider boxCollider;

    void Awake()
    {
        boxCollider = GetComponent<Collider>();
        highlight.SetActive(false);
        if (PlayerManager._instance)
            playerPosition = PlayerManager._instance.gameObject;
        else
            playerPosition = Constellation_Manager._instance.gameObject;
    }

    public void Start()
    {
        PlaceStar();
    }

    public void PlaceStar()
    {
        if(starPosition == null || playerPosition == null)
        {
            Debug.Log("positions not assigned, aborted");
            return;
        }

        transform.position = Vector3.Lerp(playerPosition.transform.position, starPosition.transform.position, distance);

        transform.forward = (gameObject.transform.position - playerPosition.transform.position).normalized;
    }

    public void SetStarDistance()
    {

        if (starGrp.isActive && !GetComponentInParent<Constellation>().isCompleted)
        {
            distance = Constellation_Manager._instance.starDistance * Constellation_Manager._instance.starDistMultiplier;
        }
        else
        {
            distance = Constellation_Manager._instance.starDistance;
        }
                
    }

    public void StarCollected(bool isCollected)
    {
        highlight.SetActive(isCollected);
        circle.color = starSprite.color;
        boxCollider.enabled = !isCollected;
        
    }

    public void DimVFX(bool isDimmed)
    {
        
        circle.gameObject.SetActive(!isDimmed);

        if(isDimmed)
            starSprite.color = new Color(starSprite.color.r, starSprite.color.g, starSprite.color.b, .5f);
        else
            starSprite.color = new Color(starSprite.color.r, starSprite.color.g, starSprite.color.b, 1f);
    }
}
