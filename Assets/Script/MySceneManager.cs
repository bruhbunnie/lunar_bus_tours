﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour
{
    public static MySceneManager _instance;

    Coroutine sceneControls;
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (_instance == null)
            _instance = this;
        if (_instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        SceneKeyPadControls(true);
    }

    public void SceneKeyPadControls(bool areOn)
    {
        if (areOn)
        {
            if (sceneControls != null)
                StopCoroutine(sceneControls);

            sceneControls = StartCoroutine(SceneLoadRoutine());
        }
        else
        {
            if (sceneControls != null)
                StopCoroutine(sceneControls);
        }
    }

    IEnumerator SceneLoadRoutine()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                CapernicusCrater();

            if (Input.GetKeyDown(KeyCode.Alpha2))
                SeaOfTranquility();

            if (Input.GetKeyDown(KeyCode.Alpha3))
                MonsHuygens();

            yield return null;
        }
    }

    public void CapernicusCrater()
    {
        SceneManager.LoadScene("01_CapernicusCrater", LoadSceneMode.Single);
        ConstellationUI_Controller._instance.ResetVariables();
    }

    public void SeaOfTranquility()
    {
        SceneManager.LoadScene("02_SeaOfTranquility", LoadSceneMode.Single);
        ConstellationUI_Controller._instance.ResetVariables();
    }

    public void MonsHuygens()
    {
        SceneManager.LoadScene("03_MonsHuygens", LoadSceneMode.Single);
        ConstellationUI_Controller._instance.ResetVariables();
    }
}
