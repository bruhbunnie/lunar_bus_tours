﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public static CameraControl _instance;
    //public Camera thirdPersonCam;
    public Transform thirdPersonCamPos;
    public Transform firstPersonCamPos;
    public Camera playerCamera;

    //public Canvas thirdPersonUI;
    public List <Canvas> firstPersonUIs;

    public List<SkinnedMeshRenderer> thirdPersonMeshs = new List<SkinnedMeshRenderer>();
    public List<MeshRenderer> firstPersonMeshs = new List<MeshRenderer>();

    public bool isOnBuss;

    Coroutine camSwitchRoutine;

    public void Awake()
    {
        _instance = this;
        EnableCameraControl(true);
        PlayerVisualInitialize();
    }

    public void Start()
    {
        //InitializeCamera();
        
        
    }

    public void PlayerVisualInitialize()
    {
        playerCamera.transform.position = thirdPersonCamPos.position;

        for (int i = 0; i < thirdPersonMeshs.Count; i++)
        {
            thirdPersonMeshs[i].enabled = true;
        }

        for (int i = 0; i < firstPersonMeshs.Count; i++)
        {
            firstPersonMeshs[i].enabled = false;
        }

        for (int i = 0; i < firstPersonUIs.Count; i++)
        {
            firstPersonUIs[i].enabled = false;
        }
    }

    void PlayerVisualSwitch()
    {
        for (int i = 0; i < thirdPersonMeshs.Count; i++)
        {
            thirdPersonMeshs[i].enabled = !thirdPersonMeshs[i].enabled;
        }

        for (int i = 0; i < firstPersonMeshs.Count; i++)
        {
            firstPersonMeshs[i].enabled = !firstPersonMeshs[i].enabled;
        }

        for (int i = 0; i < firstPersonUIs.Count; i++)
        {
            firstPersonUIs[i].enabled = !firstPersonUIs[i].enabled;
        }
    }

    public void EnableCameraControl(bool isEnabled)
    {
        if (isEnabled)
        {
            if (camSwitchRoutine != null)
            {
                StopCoroutine(camSwitchRoutine);
            }

            camSwitchRoutine = StartCoroutine(CameraControlRoutine());
        }
        else
        {
            if (camSwitchRoutine != null)
            {
                StopCoroutine(camSwitchRoutine);
            }
        }
    }

    public void ThirdPersonView()
    {
        //PlayerVisualSwitch();
        for (int i = 0; i < thirdPersonMeshs.Count; i++)
        {
            thirdPersonMeshs[i].enabled = true;
        }

        for (int i = 0; i < firstPersonMeshs.Count; i++)
        {
            firstPersonMeshs[i].enabled = false;
        }

        for (int i = 0; i < firstPersonUIs.Count; i++)
        {
            firstPersonUIs[i].enabled = false;
        }

        //thirdpersonview
        playerCamera.transform.position = thirdPersonCamPos.position;
        GetComponent<WASDMovement>().speed = 6f;
        if (Constellation_Manager._instance)
        {
            ConstellationCompare._instance.playerStars.Clear();
            Constellation_Manager._instance.ConstellationHighlightClear();
            ConstellationCompare._instance.CompareUpdater(false);
            StarCaster._instance.StarCast(false);
            //GetComponent<NodeUICaster>().NodeUICast(false);
        }
    }

    public void FirstPersonView()
    {
        //PlayerVisualSwitch();

        for (int i = 0; i < thirdPersonMeshs.Count; i++)
        {
            thirdPersonMeshs[i].enabled = false;
        }

        for (int i = 0; i < firstPersonMeshs.Count; i++)
        {
            firstPersonMeshs[i].enabled = true;
        }

        for (int i = 0; i < firstPersonUIs.Count; i++)
        {
            firstPersonUIs[i].enabled = true;
        }

        //FirstPersonView
        playerCamera.transform.position = firstPersonCamPos.position;
        GetComponent<WASDMovement>().speed = 2f;
        StarCaster._instance.StarCast(true);
        if (Constellation_Manager._instance)
        {
            if (Constellation_Manager._instance.constellationRebuild.Count > 0)
            {
                ConstellationCompare._instance.CompareUpdater(true);
            }
        }
        if (isOnBuss)
        {
            StarCaster._instance.StarCast(false);

            for (int i = 0; i < firstPersonMeshs.Count; i++)
            {
                firstPersonMeshs[i].enabled = false;
            }

            for (int i = 0; i < firstPersonUIs.Count; i++)
            {
                firstPersonUIs[i].enabled = false;
            }

            firstPersonUIs[0].enabled = true;
        }
            
    }

    IEnumerator CameraControlRoutine()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.C) && !isOnBuss)
            {
                CameraMover._instance.mouseSensativity = 100;

                UIOverlayManager._instance.ReduceCameraButtonVisability();

                if (playerCamera.transform.position != thirdPersonCamPos.position)
                {
                    ThirdPersonView();
                }
                else
                {
                    FirstPersonView();
                }      
            }
            yield return null;
        }

    }
}
