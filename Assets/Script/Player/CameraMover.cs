﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public static CameraMover _instance;

    public float mouseSensativity = 50f;

    float xRotation = 0f;

    public GameObject playerPivot;

    public GameObject firstPersonCamObject;

    public JoystickControls joystickControls;

    Coroutine moveCameraRoutine;

    public float defaultSensitivity = 50f;

    public float castingSensitivity = 20f;

    public float cameraXLimit; 

    public float touchSensitivity = 1f;

    public float prevPos;

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        MoveCamera();
        prevPos = playerPivot.transform.rotation.x;
    }

    void CheckForMove()
    {
        if (prevPos != playerPivot.transform.rotation.y)
        {
            // Do something
            UIOverlayManager._instance.MouseLookUIVisibility();
            prevPos = playerPivot.transform.rotation.y;
        }
    }

        public void MoveCamera()
    {
        if (moveCameraRoutine != null)
            StopCoroutine(moveCameraRoutine);

        moveCameraRoutine = StartCoroutine("MoveCameraRoutine");
    }

    IEnumerator MoveCameraRoutine()
    {
        yield return new WaitForSeconds(1);

        while (true)
        {
            CheckForMove();

            /*
            if (!firstPersonCamObject.GetComponent<Camera>().enabled)
            {
                touchSensitivity = 1f;
            }
            else
            {
                touchSensitivity = .5f;
            }
            */

            float mouseX = Input.GetAxis("Mouse X") * mouseSensativity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensativity * Time.deltaTime;

            xRotation -= mouseY;
            //xRotation -= joystickControls.joystickVec.y * mouseSensativity;

            if (CameraControl._instance.playerCamera.transform.position == CameraControl._instance.firstPersonCamPos.position)
                xRotation = Mathf.Clamp(xRotation, -90f, 90f);
            else
                xRotation = Mathf.Clamp(xRotation, -30f, 90f);

            playerPivot.transform.Rotate(Vector3.up * mouseX);
            //playerPivot.transform.Rotate(Vector3.up * joystickControls.joystickVec.x * mouseSensativity);

            firstPersonCamObject.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

            yield return null;
        }
    }

}
