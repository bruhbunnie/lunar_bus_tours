﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("player is inside me");

        UIOverlayManager._instance.ExitShipUI(true);
        ShipManager._instance.willExitShip = true;

        if (ShipManager._instance.exitShipRoutine != null)
        {
            StopCoroutine(ShipManager._instance.exitShipRoutine);
        }

        ShipManager._instance.exitShipRoutine = StartCoroutine(ShipManager._instance.ExitShipRoutine());
    }

    private void OnTriggerExit(Collider other)
    {
        UIOverlayManager._instance.ExitShipUI(false);

        if (ShipManager._instance.exitShipRoutine != null)
        {
            StopCoroutine(ShipManager._instance.exitShipRoutine);
        }
    }
}
