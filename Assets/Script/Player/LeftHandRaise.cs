﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftHandRaise : MonoBehaviour
{

    public GameObject endPosition;

    public float smooth = .5f;

    private void Start()
    {
        StartCoroutine(RaiseHornsRoutine());
    }

    public void RaiseHorns()
    {

    }

    IEnumerator RaiseHornsRoutine()
    {
        while (true)
        {
            gameObject.RotateTo(endPosition.transform.localEulerAngles, Time.deltaTime * smooth, 0f);

            yield return null;       
        }
    }
}
