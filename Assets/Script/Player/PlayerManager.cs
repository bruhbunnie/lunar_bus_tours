﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager _instance;

    public TMP_Text starCollected;

    public TMP_Text starCount;

    public Camera mainCamera;

    public GameObject playerBadge;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (_instance == null)
            _instance = this;
        if (_instance != this)
            Destroy(gameObject);
    }
}
