﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManager : MonoBehaviour
{
    public static ShipManager _instance;

    public GameObject playerObject;

    public Transform interiorSpawn;
    public Transform exteriorSpawn;

    public GameObject shipExteriorMesh;
    public GameObject shipInteriorMesh;
    public GameObject busControls;

    bool willEnterShip;
    public bool willExitShip;

    public Coroutine exitShipRoutine;
    Coroutine enterShipRoutine;

    public Collider collisionTrigger;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (_instance == null)
            _instance = this;
        if (_instance != this)
            Destroy(gameObject);
    }

    public void Start()
    {
        //shipInteriorMesh.SetActive(false);
        EnterShip();
    }

    private void OnTriggerEnter(Collider other)
    {
        UIOverlayManager._instance.EnterShipUI(true);
        willEnterShip = true;

        if (enterShipRoutine != null)
        {
            StopCoroutine(enterShipRoutine);
        }

        enterShipRoutine = StartCoroutine(EnterShipRoutine());
    }

    private void OnTriggerExit(Collider other)
    {
        UIOverlayManager._instance.EnterShipUI(false);

        if (enterShipRoutine != null)
        {
            StopCoroutine(enterShipRoutine);
        }
    }

    IEnumerator EnterShipRoutine()
    {
        while (willEnterShip)
        {
            if (Input.GetMouseButtonDown(0))
            {
                EnterShip();
                willEnterShip = false;
            }

            yield return null;
        }
    }

    public IEnumerator ExitShipRoutine()
    {
        while (willExitShip)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ExitShip();
                willExitShip = false;
            }

            yield return null;
        }
    }

    void EnterShip()
    {
        //UIOverlayManager._instance.WASDButtonsUI(true);
        playerObject.GetComponent<CharacterController>().enabled = false;
        collisionTrigger.enabled = false;
        shipInteriorMesh.SetActive(true);
        UIOverlayManager._instance.EnterShipUI(false);
        UIOverlayManager._instance.ExitShipUI(false);
        playerObject.transform.localPosition = interiorSpawn.transform.position;
        playerObject.transform.rotation = interiorSpawn.transform.rotation;
        playerObject.GetComponent<CharacterController>().enabled = true;

        CameraControl._instance.isOnBuss = true;
        CameraControl._instance.FirstPersonView();

        UIOverlayManager._instance.CameraButtonUI(false);
        UIOverlayManager._instance.FlagButtonUI(false);
        UIOverlayManager._instance.ConstellationButtonUI(false);
        
    }

    void ExitShip()
    {
        playerObject.GetComponent<CharacterController>().enabled = false;
        collisionTrigger.enabled = true;
        shipInteriorMesh.SetActive(false);
        UIOverlayManager._instance.ExitShipUI(false);
        playerObject.transform.localPosition = exteriorSpawn.transform.position;
        playerObject.transform.rotation = exteriorSpawn.transform.rotation;
        playerObject.GetComponent<CharacterController>().enabled = true;

        CameraControl._instance.isOnBuss = false;
        CameraControl._instance.PlayerVisualInitialize();
        CameraControl._instance.GetComponent<WASDMovement>().speed = 6f;

        UIOverlayManager._instance.CameraButtonUI(true);
        //UIOverlayManager._instance.ConstellationButtonUI(true);

        if (ConsoleBlinker._instance.awayConParticle.activeInHierarchy)
            ConsoleBlinker._instance.awayConParticle.SetActive(false);
    }
}
