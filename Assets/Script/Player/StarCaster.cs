﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StarCaster : MonoBehaviour
{
    public static StarCaster _instance;

    [SerializeField] RaycastHit theObject;
    [SerializeField] GameObject hitTarget;
    public GameObject cameraObj;

    Coroutine starCasterRoutine;

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        //StarCast(true);
    }

    public void StarCast(bool isCasting)
    {
        if (!isCasting && starCasterRoutine != null)
            StopCoroutine(starCasterRoutine);
        else
        {
            if (starCasterRoutine != null)
                StopCoroutine(starCasterRoutine);

            starCasterRoutine = StartCoroutine(StarCastRoutine());
        }
    }

    IEnumerator StarCastRoutine()
    {
        while (Constellation_Manager._instance)
        {
            if (cameraObj.GetComponent<Camera>().enabled  && Constellation_Manager._instance.constellationRebuild.Count > 0)
            {
                if (Input.GetMouseButton(0))
                {
                    StarObjectSearch();
                    CameraMover._instance.mouseSensativity = CameraMover._instance.castingSensitivity;
                }
                else
                {
                    CameraMover._instance.mouseSensativity = CameraMover._instance.defaultSensitivity;
                }
            }

            yield return null;
        }
    }

    private void StarObjectSearch()
    {
        RaycastHit[] hitObjects;
        hitObjects = Physics.RaycastAll(cameraObj.transform.position, cameraObj.transform.TransformDirection(Vector3.forward), 600);

        for (int i = 0; i < hitObjects.Length; i++)
        {
            if (hitObjects[i].transform.GetComponent<StarPositioner>() != null)
            {

                if (StarBelongstoGroup(hitObjects[i]))
                {
                    hitObjects[i].transform.GetComponent<StarPositioner>().StarCollected(true);
                    ConstellationCompare._instance.playerStars.Add(hitObjects[i].transform.gameObject);
                    AudioManager._instance.StarSelect();
                    ConstellationUI_Controller._instance.currentConstellation.LineArt();

                    if (!UIOverlayManager._instance.lookUpComplete)
                    {
                        UIOverlayManager._instance.FadeLookUpObjects();
                    }
                }
                else
                {
                    Debug.Log("Play the bad audio");
                    Debug.Log("Highlight the bad way");  
                }


            }
        }
    }

    private bool StarBelongstoGroup(RaycastHit hitStar)
    {
        bool foundIt = false;

        for (int i = 0; i < ConstellationUI_Controller._instance.currentConstellation.starsObj.Count; i++)
        {
            if (hitStar.transform.name == ConstellationUI_Controller._instance.currentConstellation.starsObj[i].name)
            {
                foundIt = true;
                break;
            }   
        }

        return foundIt;
    }

}
