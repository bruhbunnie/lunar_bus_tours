﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WASDMovement : MonoBehaviour
{
    public CharacterController controller;
    bool isGrounded;
    public Transform groundCheck;
    public float groundDistance = 0.1f;
    public LayerMask groundMask;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    Vector3 velocity;

    public JoystickControls joystickControls;

    public Animator animator;

    public float speed = 12f;

    Coroutine moveControlRoutine;

    private void Start()
    {
        MoveControl(true);
    }

    public void MoveControl(bool isMoving)
    {
        if (!isMoving)
            StopCoroutine(moveControlRoutine);
        else
        {
            if (!isMoving)
                StopCoroutine(moveControlRoutine);
            moveControlRoutine = StartCoroutine("MoveControlRoutine");
        }
    }

    IEnumerator MoveControlRoutine()
    {
        yield return new WaitForSeconds(1);
        while (true)
        {
            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
                animator.SetBool("isGrounded", true);
            }
            else
            {
                animator.SetBool("isGrounded", false);
            }

            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            if(Constellation_Manager._instance)
                Constellation_Manager._instance.ConstellationPositionUpdate();

            Vector3 move = transform.right * x + transform.forward * y;

            //move = transform.right * joystickControls.joystickVec.x + transform.forward * joystickControls.joystickVec.y;

            controller.Move(move * speed * Time.deltaTime);

            if ((Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(1)) && isGrounded && !CameraControl._instance.isOnBuss)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
               
                animator.SetTrigger("jumpTrigger");

                UIOverlayManager._instance.FindWASD("Spacebar");
            }

            if (x == 0 &&  y == 0 )
            {
                animator.SetBool("isRunning", false);
            }
            else
            {
                animator.SetBool("isRunning", true);
            }
            velocity.y += gravity * Time.deltaTime;

            controller.Move(velocity * Time.deltaTime);

            //Jumping
            animator.SetFloat("velocity", velocity.y);

            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
                UIOverlayManager._instance.FindWASD("W");
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                UIOverlayManager._instance.FindWASD("A");
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
                UIOverlayManager._instance.FindWASD("S");
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                UIOverlayManager._instance.FindWASD("D");

            if (Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.RightArrow))
                Application.Quit();


            yield return null;
        }
    }

    /*
    void JoystickMovement()
    {
        if (joystickControls.joystickVec.y != 0)
        {
            rb.velocity = new Vector2(joystickControls.joystickVec.x * speed, joystickControls.joystickVec.y * speed);
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }
    */
}
