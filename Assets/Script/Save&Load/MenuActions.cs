﻿using System.Collections.Generic;
using UnityEngine;

public class MenuActions : MonoBehaviour
{
    [SerializeField] public List<SwitchData> switchesData = new List<SwitchData>();

    public GameObject switchesGrp;

    public void CollectInformation()
    {
        switchesData.Clear();

        for (int i = 0; i < switchesGrp.transform.childCount; i++)
        {
            Debug.Log("For looping " + i);

            if (switchesGrp.transform.GetChild(i).GetComponent<TrueOrFalse>() != null)
            {
                switchesData.Add(new SwitchData(switchesGrp.transform.GetChild(i).name, switchesGrp.transform.GetChild(i).GetComponent<TrueOrFalse>().isSquareGreen));
            }
                

        }
    }

    public void ClearData()
    {
        switchesData.Clear();
    }

    public void SaveSwitches()
    {
#if UNITY_WEBGL
        SaveSystem.WebGLSaveData(this);
#else
            SaveSystem.SaveData(this);
#endif
    }

    public void LoadSwitches()
    {
#if UNITY_WEBGL
        PlayerData_WGL data = SaveSystem.WebGLLoadData();
#else
            PlayerData data = SaveSystem.LoadData();
#endif

        switchesData.Clear();

        //switchesData = new List<SwitchData>;
        for (int i = 0; i < data.switches.Length; i++)
        {
            Debug.Log("For looping " + i);

            switchesData.Add(new SwitchData(switchesGrp.transform.GetChild(i).name, data.switches[i]));

            switchesGrp.transform.GetChild(i).GetComponent<TrueOrFalse>().SetSquare(data.switches[i]);
        }
    }
}