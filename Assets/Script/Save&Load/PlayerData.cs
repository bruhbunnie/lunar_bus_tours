﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public bool[] switches;

    // No, this pulls apart all fo the data in the file and adds it here
    public PlayerData(MenuActions menuData)
    {
        switches = new bool[menuData.switchesData.Count];

        for (int i = 0; i < switches.Length; i++)
        {
            switches[i] = menuData.switchesData[i].switchValue;
        }
    }
}
