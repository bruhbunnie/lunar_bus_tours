﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;

public static class SaveSystem 
{
    //private PlayerData playerData;

    public static void SaveData(MenuActions collectedData)
    {

        BinaryFormatter formatter = new BinaryFormatter();

        string path = Application.persistentDataPath + "/player.fun";

        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData saveData = new PlayerData(collectedData);

        formatter.Serialize(stream, saveData);

        stream.Close();
    }

    public static void WebGLSaveData(MenuActions collectedData)
    {
        string giant;

        XmlSerializer serializer = new XmlSerializer(typeof(PlayerData_WGL));

        using (StringWriter sw = new StringWriter())
        {
            PlayerData_WGL saveData = new PlayerData_WGL();

            saveData.switches = new bool[collectedData.switchesData.Count];

            for (int i = 0; i < collectedData.switchesData.Count; i++)
            {
                saveData.switches[i] = collectedData.switchesData[i].switchValue;
            }

            serializer.Serialize(sw, saveData);

            PlayerPrefs.SetString("playerSwitches", sw.ToString());

            giant = sw.ToString();
        }

        Debug.Log("success?");
    }

    public static PlayerData LoadData()
    {
        string path = Application.persistentDataPath + "/player.fun";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.Log("save file not found in " + path);
            return null;
        }
    }

    public static PlayerData_WGL WebGLLoadData()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(PlayerData_WGL));

        string text = PlayerPrefs.GetString("playerSwitches");

        if (text.Length != 0)
        {
            using (var reader = new System.IO.StringReader(text))
            {
                PlayerData_WGL data = serializer.Deserialize(reader) as PlayerData_WGL;
                return data;
            }
        }
        else
        {
            Debug.Log("save file not found");
            return null;
        }
    }
}
