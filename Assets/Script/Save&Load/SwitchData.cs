﻿[System.Serializable]
public struct SwitchData
{
    public string objectName;
    public bool switchValue;

    public SwitchData(string objectName, bool switchValue)
    {
        this.objectName = objectName;

        this.switchValue = switchValue;
    }
}
