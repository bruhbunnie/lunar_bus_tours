﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrueOrFalse : MonoBehaviour
{
    public bool isSquareGreen = false;

    public Image colourSquare;

    // Start is called before the first frame update
    void Start()
    {
        colourSquare.color = new Color(1, 0, 0);
    }

    public void ButtonSwitch()
    {
        isSquareGreen = !isSquareGreen;
        
        if (isSquareGreen)
        {
            colourSquare.color = new Color(0,1,0);
        }
        else
        {
            colourSquare.color = new Color(1, 0, 0);
        }
    }

    public void SetSquare(bool isSquareGreen)
    {
        this.isSquareGreen = isSquareGreen;

        if (this.isSquareGreen)
        {
            colourSquare.color = new Color(0, 1, 0);
        }
        else
        {
            colourSquare.color = new Color(1, 0, 0);
        }
    }
}
