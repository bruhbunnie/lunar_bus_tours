﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadManager : MonoBehaviour
{
    public static SaveLoadManager _instance;

    //Player BadgeImages 4 String names
    //PlayerBadge Colours Vector 4

    public string badgeBckg;
    public Vector4 badgeBckgColor;

    public string badgeBorder;
    public Vector4 badgeBorderColor;

    public string badegeDe1;
    public Vector4 badgeDe1Color;

    public string badgeDe2;
    public Vector4 badgeDe2Color;
    

    //Scene 1 name
    // location nodesComplete bools
    public List<bool> capCraterNodes = new List<bool>();
    // location complete bool
    public bool capCraterComplete;

    //Scene 2 name
    // location nodesComplete bools
    // location complete bool

    //Scene 3 name
    // location nodesComplete bools
    // location complete bool

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (_instance == null)
            _instance = this;
        if (_instance != this)
            Destroy(gameObject);
    }


       
    public void PullBadgeData()
    {

    }

    public void PushBadgeData()
    {

    }
}
