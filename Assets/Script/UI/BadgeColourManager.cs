﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadgeColourManager : MonoBehaviour
{
    public static BadgeColourManager _instance;

    [SerializeField] List<ColourButton> colourButtons = new List<ColourButton>();

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        CollectColourButtons();
    }

    void CollectColourButtons()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            colourButtons.Add(transform.GetChild(i).GetComponent<ColourButton>());
        }
    }

    public void SetToSelectedColour(Color colour)
    {
        SetButtonsToDefault();
        CorrectHighlight(colour);
    }

    public void SetButtonsToDefault()
    {
        for (int i = 0; i < colourButtons.Count; i++)
        {
            colourButtons[i].isActive = false;
            colourButtons[i].OnDefault();
        }
    }

    void CorrectHighlight(Color colour)
    {
        for (int i = 0; i < colourButtons.Count; i++)
        {
            if (colourButtons[i].button.color == colour)
            {
                colourButtons[i].SetToActive();
                return;
            }
        }
    }
}
