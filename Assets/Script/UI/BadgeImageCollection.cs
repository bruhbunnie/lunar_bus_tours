﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadgeImageCollection : MonoBehaviour
{
    //public static BadgeCollection _instance;

    public List<Sprite> badgeBackgrounds;
    public List<Sprite> badgeBorders;
    public List<Sprite> badgeDetails;

    private void Awake()
    {
        CollectImages();
    }

    private void Start()
    {
        
    }
    public void CollectImages()
    {
        badgeBackgrounds.AddRange(Resources.LoadAll<Sprite>("Badges/Background"));

        badgeBorders.AddRange(Resources.LoadAll<Sprite>("Badges/Border"));

        badgeDetails.AddRange(Resources.LoadAll<Sprite>("Badges/Detail"));
    }
}
