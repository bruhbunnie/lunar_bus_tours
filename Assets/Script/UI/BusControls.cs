﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusControls : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<NodeUICaster>().NodeUICast(true);
    }

    private void OnTriggerExit(Collider other)
    {
        other.GetComponent<NodeUICaster>().NodeUICast(false);
    }
}
