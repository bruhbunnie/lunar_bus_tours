﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColourButton : MonoBehaviour, IuiActivate
{
    public Image highlight;

    public Image button;

    public Color inactiveColour;
    public Color highlightColour;
    //public Color activateColour;

    public bool isActive;

    public Collider buttonCollider;

    void Start()
    {
        highlight.color = inactiveColour;
        buttonCollider = GetComponent<Collider>();
    }

    public void SetToActive()
    {
        //isActive = true;
        //highlight.color = highlightColour;
        
    }

    public void OnActivated()
    {
        //if (isActive)
            //return;

        BadgeColourManager._instance.SetButtonsToDefault();
        isActive = true;

        BadgeManager._instance.ChangeLayerColour(button.color);

        if (BadgeRenderManager._instance != null)
            BadgeRenderManager._instance.MakeSprite();
    }

    public void OnHighlight()
    {
        highlight.color = highlightColour;

    }

    public bool IsLit()
    {
        if (highlight.color == highlightColour)
            return true;
        else
            return false;
    }


    public void OnDefault()
    {
        //if (isActive)
          //  return;

        highlight.color = inactiveColour;

    }
}
