﻿using UnityEngine;

public interface IuiActivate 
{
    void OnActivated();

    void OnHighlight();

    bool IsLit();

    void OnDefault();


}
