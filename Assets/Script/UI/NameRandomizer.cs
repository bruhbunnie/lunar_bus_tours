﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NameRandomizer : MonoBehaviour
{
    NamesList namesList;

    public TMP_Text textBox;

    public string nameString;

    private void Awake()
    {
        namesList = GetComponent<NamesList>();
    }



    public void CreateRandomName()
    {
        string first = namesList.describersList[Random.Range(0, namesList.describersList.Length - 1)];

        string second = namesList.nounsList[Random.Range(0, namesList.nounsList.Length - 1)];

        nameString = first + " " + second;


        textBox.text = nameString.Replace("\r", "").Replace("\n", "");

        BadgeManager._instance.UpdateBadgeText();
    }
}
