﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class NamesList : MonoBehaviour
{
    public string[] nounsList;

    public string[] describersList;

    public TextAsset nounsText;

    public TextAsset describersText;

    // Start is called before the first frame update
    void Start()
    {
        nounsList = nounsText.text.Split("\n"[0]);

        describersList = describersText.text.Split("\n"[0]);
    }
}
