﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Node : MonoBehaviour
{
    public Constellation nodeConstellation;

    public string nodeName;

    public GameObject playerObject;

    public TMP_Text nodeNamePlate;

    public List<ObjectMover> uiObjectMovers;

    public Collision playerDetect;

    public SpriteRenderer btnConstellationIcon;

    public List<SpriteRenderer> lockRender;

    public List<ObjectMover> LockMover;

    public List<NodeAudioBtn_i> nodeButtons = new List<NodeAudioBtn_i>();

    Coroutine playerClose;

    public bool isActive;

    public List<MeshRenderer> beams = new List<MeshRenderer>();

    public Material constComplete;

    // animation time
    public float moveTime = 1f;

    // delay before iTween animation begins
    public float delay = 0f;

    // ease in-out
    public iTween.EaseType easeType = iTween.EaseType.easeOutExpo;

    private void Awake()
    {

    }

    private void Start()
    {
        if (Constellation_Manager._instance)
        {
            btnConstellationIcon.sprite = nodeConstellation.constellationIcon;

            nodeNamePlate.text = nodeConstellation.contellationName.ToString();

            nodeName = nodeConstellation.contellationName.ToString();

            nodeButtons.AddRange(GetComponentsInChildren<NodeAudioBtn_i>());
        }

        AudioButtonCollision(false);
    }

    void AudioButtonCollision(bool isActive)
    {
        foreach (NodeAudioBtn_i button in nodeButtons)
        {
            button.buttonCollider.enabled = isActive;
        }
    }

    public void UIStartPosition()
    {
        foreach (ObjectMover mover in uiObjectMovers)
        {
            mover.mode = GraphicMoverMode.MoveToHide;
            mover.moveTime = 0f;
            mover.easeType = iTween.EaseType.linear;
            mover.delay = 0f;

            mover.Move();
        }
    }

    public void UIHidePosition()
    {
        foreach (ObjectMover mover in uiObjectMovers)
        {
            mover.mode = GraphicMoverMode.MoveToHide;
            mover.moveTime = moveTime;
            mover.easeType = easeType;
            mover.delay = delay;

            mover.Move();
        }
    }

    public void UIShowPosition()
    {
        foreach (ObjectMover mover in uiObjectMovers)
        {
            mover.mode = GraphicMoverMode.MoveToShow;
            mover.moveTime = moveTime;
            mover.easeType = easeType;
            mover.delay = delay;

            mover.Move();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        playerObject = other.gameObject;

        isActive = true;

        other.GetComponent<NodeUICaster>().NodeUICast(true);

        GetComponentInChildren<NodeButton_i>().OnActivated();

        ConstellationUI_Controller._instance.SetConstellation(nodeConstellation.contellationName);

        if (playerClose != null)
            StopCoroutine(playerClose);

        playerClose = StartCoroutine(PlayerClose());

        if (!UIOverlayManager._instance.lookUpObjects[0].activeSelf)
        {
            UIOverlayManager._instance.DisplayLookUpObjects(true);
        }


    }



    private IEnumerator PlayerClose()
    {
        CameraControl._instance.FirstPersonView();

        while (true)
        {
            bool isShowing = false;

            if (nodeConstellation.isCompleted && !isShowing)
            {
                isShowing = true;
                UIShowPosition();
                ChangeBacklight();
            }

            AudioButtonCollision(nodeConstellation.isCompleted);

            yield return null;
        }
    }

    public void ChangeBacklight()
    {
        foreach (MeshRenderer beam in beams)
        {
            beam.material = constComplete;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        UIHidePosition();

        playerObject = null;

        isActive = false;

        AudioButtonCollision(false);

        CameraControl._instance.ThirdPersonView();

        StopCoroutine(playerClose);

        if (other.GetComponent<NodeUICaster>().savedHitTarget != null && other.GetComponent<NodeUICaster>().savedHitTarget.GetComponent<NodeButton>() != null)
            other.GetComponent<NodeUICaster>().savedHitTarget.GetComponent<NodeButton>().UnHighlightSprites();

        else if (other.GetComponent<NodeUICaster>().savedHitTarget != null && other.GetComponent<NodeUICaster>().savedHitTarget.GetComponent<IuiActivate>() != null)
        {
            other.GetComponent<NodeUICaster>().savedHitTarget.GetComponent<IuiActivate>().OnDefault();
            other.GetComponent<NodeUICaster>().savedHitTarget = null;
        }
            
        other.GetComponent<NodeUICaster>().NodeUICast(false);
    }
}