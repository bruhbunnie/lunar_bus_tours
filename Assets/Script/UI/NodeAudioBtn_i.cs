﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NodeAudioBtn_i : MonoBehaviour, IuiActivate
{
    public Image buttonSprite;

    public List<Sprite> audioSprites;

    public AudioSource audioSource;

    Color highlightColour = new Color(0.9245283f, 0.2561061f, 0f, 1f);
    Color defaultColour = new Color(0.3803922f, 1f, 0.8118201f, 1f);
    Color playingColour = new Color(0.02922988f, 1f, 0f, 1f);

    public Collider buttonCollider;

    //bool isPlaying = false;

    Coroutine audioPlayingRoutine;

    public int constIndex;
    public int trackIndex;

    public AudioClip recording;

    public bool isLit;

    void Awake()
    {
        buttonSprite = GetComponent<Image>();
        buttonSprite.color = defaultColour;
        buttonCollider = GetComponent<Collider>();
    }

    private void Start()
    {
        if(AudioManager._instance != null)
        {
            recording = AudioManager._instance.Recording(constIndex, trackIndex);
            audioSource = AudioManager._instance.nodeSource;
        }
        audioSprites[0] = buttonSprite.sprite;
    }

    public void SetToActive()
    {

    }

    public void OnActivated()
    {
        if(audioSource.clip != recording)
            audioSource.clip = recording;

        if (!audioSource.isPlaying)
        {
            Play();
        }
        else
        {
            
            Stop();
        }
    }

    private void Stop()
    {
        if (audioSource.clip == recording)
            audioSource.Stop();

        buttonSprite.sprite = audioSprites[0];

        if(PlayerManager._instance.GetComponent<NodeUICaster>().hitTarget == gameObject)
            OnHighlight();
        //StopCoroutine(audioPlayingRoutine);
    }


    private void Play()
    {
        audioSource.Play();
        buttonSprite.sprite = audioSprites[1];
        buttonSprite.color = playingColour;

        if (audioPlayingRoutine != null)
            StopCoroutine(audioPlayingRoutine);

        audioPlayingRoutine = StartCoroutine(PlayingClipRoutine());

    }

    private IEnumerator PlayingClipRoutine()
    {
        while (audioSource.clip == recording && audioSource.isPlaying)
        {
            yield return null;
        }
        //buttonSprite.sprite = audioSprites[0];
        //OnDefault();

        Stop();
    }


    public void OnHighlight()
    {
        if(audioSource.clip == recording && audioSource.isPlaying)
            buttonSprite.color = playingColour;
        else
            buttonSprite.color = highlightColour;

        isLit = true;
    }

    public bool IsLit()
    {
        return isLit;
    }


    public void OnDefault()
    {
            buttonSprite.color = defaultColour;
        isLit = false;
    }
}
