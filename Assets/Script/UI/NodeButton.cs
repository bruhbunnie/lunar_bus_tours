﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum NodeButtonMode
{
    Constellation,
    Audio,
    NextBtn,
    PreviousBtn,
    Randomizer,
    Input
}

public class NodeButton : MonoBehaviour
{
    public NodeButtonMode mode;

    public List<SpriteRenderer> buttonSprites;

    public List<Image> buttonImages;

    public List<Sprite> audioSprites;

    public Color highlight;

    public Color unhighlight;

    public Color clicked;

    private Constellation constellation;

    private SpriteRenderer sprite;

    public AudioSource audioSource;

    public bool isConstellationActive;

    public Collider objCollider;

    public TMP_InputField textInput;

    public NameRandomizer nameRandomizer;

    private void Awake()
    {
        if (mode == NodeButtonMode.Audio)
            sprite = GetComponentInChildren<SpriteRenderer>();

        objCollider = GetComponent<Collider>();
    }

    public void HighlightSprites()
    {
        if (mode == NodeButtonMode.Audio)
        {
            SpriteColours("highlight");
            return;
        }

        if (isConstellationActive)
            return;

        if (buttonSprites.Count > 0)
        {
            foreach (SpriteRenderer sprite in buttonSprites)
            {
                sprite.color = new Color(highlight.r, highlight.g, highlight.b, highlight.a);
            }
        }
        else if (buttonImages.Count > 0)
        {
            foreach (Image image in buttonImages)
            {
                image.color = new Color(highlight.r, highlight.g, highlight.b, highlight.a);
            }
        }
        else
        {
            Debug.Log("no images to highlight bruh");
        }
    }

    public void UnHighlightSprites()
    {
        if (mode == NodeButtonMode.Audio)
        {
            SpriteColours("unhighlight");
            return;
        }

        if (isConstellationActive)
            return;

        if (buttonSprites.Count > 0)
        {
            foreach (SpriteRenderer sprite in buttonSprites)
            {
                sprite.color = new Color(unhighlight.r, unhighlight.g, unhighlight.b, unhighlight.a);
            }
        }
        else if (buttonImages.Count > 0)
        {
            foreach (Image image in buttonImages)
            {
                image.color = new Color(unhighlight.r, unhighlight.g, unhighlight.b, unhighlight.a);
            }
        }
        else
        {
            Debug.Log("no images to unhighlight bruh");
        }
    }

    public void NodeUIClickEvent()
    {
        switch (mode)
        {
            case NodeButtonMode.Constellation:
                if (isConstellationActive)
                    return;
                constellation = GetComponentInParent<Node>().nodeConstellation;
                AddConstellation();
                break;

            case NodeButtonMode.Audio:
                PlayMode();
                break;

            case NodeButtonMode.NextBtn:

                break;

            case NodeButtonMode.PreviousBtn:

                break;

            case NodeButtonMode.Randomizer:

                RandomizeName();
                break;

            case NodeButtonMode.Input:

                StartInput();
                break;
        }
    }

    private void StartInput()
    {
        textInput.caretWidth = 0;
        textInput.ActivateInputField();
    }

    private void RandomizeName()
    {
        if (nameRandomizer != null)
            nameRandomizer.CreateRandomName();

        if (BadgeRenderManager._instance != null)
            BadgeRenderManager._instance.MakeSprite();
    }

    private void AddConstellation()
    {
        foreach (SpriteRenderer sprite in buttonSprites)
        {
            sprite.color = new Color(clicked.r, clicked.g, clicked.b, clicked.a);
        }

        isConstellationActive = true;
        Constellation_Manager._instance.constellationRebuild.Add(constellation);

        ConstellationUI_Controller._instance.constellationIcons.Add(constellation.constellationIcon);
        ConstellationUI_Controller._instance.constellationNames.Add(constellation.contellationName);
        ConstellationUI_Controller._instance.ConstellationAdded();

        bool index = false;
        ConstellationCompare._instance.isCompleteds.Add(index);

        ConstellationCompare._instance.CompareUpdater(true);
    }

    private void PlayMode()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
            sprite.sprite = audioSprites[1];
            StartCoroutine(PlayingClipRoutine());
        }
        else
        {
            audioSource.Stop();
            sprite.sprite = audioSprites[0];
            SpriteColours("unhighlight");
        }
    }

    private IEnumerator PlayingClipRoutine()
    {
        while (audioSource.isPlaying)
        {
            yield return null;
        }
        sprite.sprite = audioSprites[0];
    }

    private void SpriteColours(string colourMode)
    {
        switch (colourMode)
        {
            case "highlight":
                sprite.color = new Color(highlight.r, highlight.g, highlight.b, highlight.a);
                break;

            case "unhighlight":
                sprite.color = new Color(unhighlight.r, unhighlight.g, unhighlight.b, unhighlight.a);
                break;

            case "clicked":
                sprite.color = new Color(clicked.r, clicked.g, clicked.b, clicked.a);
                break;
        }
    }
}