﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NodeButton_i : MonoBehaviour , IuiActivate
{
    public List<SpriteRenderer> buttonSprites = new List<SpriteRenderer>();

    public Color highlightColour;
    public Color defaultColour;
    public Color collectedColour;

    public Collider buttonCollider;

    bool isCollected;

    private Constellation constellation;

    void Awake()
    {
        buttonCollider = GetComponent<Collider>();
    }

    void Start()
    {
        SpriteListColour(defaultColour);
    }

    void SpriteListColour(Color colour)
    {
        foreach (SpriteRenderer sprite in buttonSprites)
        {
            sprite.color = colour;
        }
    }

    public void SetToActive()
    {

    }

    public void OnActivated()
    {
        if (isCollected)
            return;

        constellation = GetComponentInParent<Node>().nodeConstellation;
        AddConstellation();
    }

    private void AddConstellation()
    {
        SpriteListColour(collectedColour);

        isCollected = true;
        Constellation_Manager._instance.constellationRebuild.Add(constellation);

        ConstellationUI_Controller._instance.constellationIcons.Add(constellation.constellationIcon);
        ConstellationUI_Controller._instance.constellationNames.Add(constellation.contellationName);
        ConstellationUI_Controller._instance.ConstellationAdded();

        bool index = false;
        ConstellationCompare._instance.isCompleteds.Add(index);

        ConstellationCompare._instance.CompareUpdater(true);
    }

    public void OnHighlight()
    {
        SpriteListColour(highlightColour);
    }

    public bool IsLit()
    {
        if (isCollected)
            return true;
        else
            return false;
    }


    public void OnDefault()
    {
        if(!isCollected)
            SpriteListColour(defaultColour);
    }
}
