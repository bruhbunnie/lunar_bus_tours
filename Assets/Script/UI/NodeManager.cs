﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeManager : MonoBehaviour
{
    public List<Node> nodes;

    public List<Canvas> canvases = new List<Canvas>();

    private void Awake()
    {
        CollectNodes();
        CollectCanvases();
    }

    void Start()
    {
        StartNodeUI();
        PlaceCameraOnNodes();
    }

    void CollectNodes()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            nodes.Add(gameObject.transform.GetChild(i).GetComponent<Node>());
        }
    }

    void StartNodeUI()
    {
        foreach (Node node in nodes)
        {
                for (int i = 0; i < node.uiObjectMovers.Count; i++)
                {
                    node.UIStartPosition();
                }
            
        }
    }

    void CollectCanvases()
    {
        foreach (Node node in nodes)
        {
            canvases.AddRange(node.GetComponentsInChildren<Canvas>());
        }
    }

    void PlaceCameraOnNodes()
    {
        foreach (Canvas canvas in canvases)
        {
            if (PlayerManager._instance != null)
            {
                canvas.worldCamera = PlayerManager._instance.mainCamera;
            }
        }
    }
}
