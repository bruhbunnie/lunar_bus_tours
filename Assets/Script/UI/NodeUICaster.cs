﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NodeUICaster : MonoBehaviour
{
    [SerializeField] private RaycastHit theObject;
    public GameObject hitTarget = null;
    public GameObject savedHitTarget = null;
    public GameObject cameraObj;


    private Coroutine NodeUIRoutine;

    public void NodeUICast(bool isCasting)
    {
        if (!isCasting)
            StopCoroutine(NodeUIRoutine);
        else
        {
            if (NodeUIRoutine != null)
                StopCoroutine(NodeUIRoutine);

            NodeUIRoutine = StartCoroutine(NodeUICastRoutine());
        }
    }

    private IEnumerator NodeUICastRoutine()
    {
        while (true)
        {
            if (cameraObj.GetComponent<Camera>().enabled)
            {
                if (Physics.Raycast(cameraObj.transform.position, cameraObj.transform.TransformDirection(Vector3.forward), out theObject))
                {
                    hitTarget = theObject.transform.gameObject;

                    if (hitTarget.tag == "NodeUI")
                    {
                        savedHitTarget = hitTarget;
                        if (hitTarget.GetComponent<NodeButton>())
                            hitTarget.GetComponent<NodeButton>().HighlightSprites();


                        if (Input.GetMouseButtonDown(0))
                        {
                            AudioManager._instance.PlayerClick();
                            if (hitTarget.GetComponent<NodeButton>())
                                hitTarget.GetComponent<NodeButton>().NodeUIClickEvent();

                            //Debug.Log("We clicked");
                        }
                    }

                    if (hitTarget.tag == "UI")
                    {
                        savedHitTarget = hitTarget;

                        IuiActivate uiObject = hitTarget.GetComponent<IuiActivate>();

                        if (uiObject != null && !uiObject.IsLit())
                        {
                            uiObject.OnHighlight();
                        }

                        if (Input.GetMouseButtonDown(0) && uiObject != null)
                        {
                            AudioManager._instance.PlayerClick();
                            uiObject.OnActivated();

                        }
                    }
                }

                if (savedHitTarget != null)
                {
                    if (savedHitTarget != hitTarget && savedHitTarget.GetComponent<NodeButton>())
                        savedHitTarget.GetComponent<NodeButton>().UnHighlightSprites();

                    if (savedHitTarget != hitTarget && savedHitTarget.GetComponent<IuiActivate>() != null)
                    {
                        savedHitTarget.GetComponent<IuiActivate>().OnDefault();
                        savedHitTarget = null;
                    }

                }
            }

            if (!cameraObj.GetComponent<Camera>().enabled && savedHitTarget != null && savedHitTarget.GetComponent<NodeButton>() != null)
            {
                savedHitTarget.GetComponent<NodeButton>().UnHighlightSprites();
            }
            yield return null;
        }
    }
}