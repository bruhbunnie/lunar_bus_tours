﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PageButton : MonoBehaviour, IuiActivate
{
    public Image arrow;

    public Color defaultColour;
    public Color pressedColour;
    public Color highlightColour;
    public Color hiddenColour;

    public int listIndexMax;
    public int currentIndex;
    public bool isNextPage;

    public Collider arrowCollider;

    void Start()
    {
        //defaultColour = arrow.color;
        arrowCollider = GetComponent<Collider>();

        PageButtonManager._instance.pageButtons.Add(this);
    }

    public void HideButton()
    {
        arrow.color = hiddenColour;
        arrowCollider.enabled = false;
    }

    public void ShowButton()
    {
        arrow.color = defaultColour;
        arrowCollider.enabled = true;
    }

    public void OnActivated()
    {
        if (BadgeManager._instance.badgeLayer == BadgeLayer.EMPTY)
            return;

        UpdateIndex();
        BadgeDisplayManager._instance.UpdateBadgeImages(BadgeNColourManager._instance.badgeSorter.ListOfSpritesLists.lists[PageButtonManager._instance.currentListIndex].sprites);
    }

    void UpdateIndex()
    {
        currentIndex = PageButtonManager._instance.currentListIndex;

        if (isNextPage)
        {
            if (listIndexMax == currentIndex)
                PageButtonManager._instance.currentListIndex = 0;
            else
                PageButtonManager._instance.currentListIndex += 1;
        }
        else
        {
            if (currentIndex == 0)
                PageButtonManager._instance.currentListIndex = listIndexMax;
            else
                PageButtonManager._instance.currentListIndex -= 1;
        }
            
    }

    public void OnHighlight()
    {
        if (arrow.color == defaultColour)
        {
            arrow.color = highlightColour;
        }
    }

    public bool IsLit()
    {
        if (arrow.color == highlightColour)
            return true;
        else
            return false;
    }

    public void OnDefault()
    {
        arrow.color = defaultColour;
    }
}
