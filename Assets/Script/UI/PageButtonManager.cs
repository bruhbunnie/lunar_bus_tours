﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageButtonManager : MonoBehaviour
{
    public static PageButtonManager _instance;

    public List<PageButton> pageButtons = new List<PageButton>();

    public int currentListIndex;

    private void Awake()
    {
        _instance = this;
    }

    void HideButtons()
    {
        foreach (PageButton pageButton in pageButtons)
        {
            pageButton.HideButton();
        }
    }

    void ShowButtons()
    {

        foreach (PageButton pageButton in pageButtons)
        {
            pageButton.ShowButton();
        }
    }

    public void SetListIndex(int count)
    {
        currentListIndex = 0;

        if (count > 1)
        {
            ShowButtons();

            foreach (PageButton pageButton in pageButtons)
            {
                pageButton.listIndexMax = count - 1;
            }
        }
        else
            HideButtons();
    }
}
