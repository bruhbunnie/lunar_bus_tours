﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TrashButton : MonoBehaviour, IuiActivate
{
    public Image buttonImage;

    public TMP_Text textField;
    //public Image badge;

    //public Color inactiveColour;
    public Color highlightColour;
    public Color defaultColour;

    //public bool isActive;

    public Collider buttonCollider;

    void Start()
    {
        buttonImage.color = defaultColour;
        buttonCollider = GetComponent<Collider>();

        buttonImage =GetComponent<Image>();
    }

    public void SetToActive()
    {

    }

    public void OnActivated()
    {
        textField.text = "";

        BadgeManager._instance.UpdateBadgeText();

        if (BadgeRenderManager._instance != null)
            BadgeRenderManager._instance.MakeSprite();
    }

    public void OnHighlight()
    {
        if (buttonImage.color == defaultColour)
        {
            buttonImage.color = highlightColour;
        }
    }

    public bool IsLit()
    {
        if (buttonImage.color == highlightColour)
            return true;
        else
            return false;
    }


    public void OnDefault()
    {
        buttonImage.color = defaultColour;

    }
}
