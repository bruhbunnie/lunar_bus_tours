﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIOverlayManager : MonoBehaviour
{
    public static UIOverlayManager _instance;

    public GameObject enterShipBtn;

    public GameObject exitShipBtn;

    public GameObject flagBtn;

    public GameObject cameraBtn;

    public TMP_Text cameraText;

    public Image cameraBtnImage;

    public GameObject constellationBtn;

    public TMP_Text constellationText;

    public Image constellationBtnImage;

    public List<GameObject> wasdButtons = new List<GameObject>();

    public GameObject mouslookBtn;

    public TMP_Text mouselookText;
    public Image mouselookImage;

    float alphaAmount;

    public List<UIMover> uiMovers = new List<UIMover>();

    public List<UIFader> uiFaders = new List<UIFader>();

    public List<GameObject> lookUpObjects = new List<GameObject>();
    public bool lookUpComplete;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        if (_instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        StartCoroutine(WaitRoutine());
        //TurnOffOverlay();

        DisplayLookUpObjects(false);
        
    }

    public void DisplayLookUpObjects(bool isActive)
    {
        foreach (GameObject uiObject in lookUpObjects)
        {
            uiObject.SetActive(isActive);
        }
    }

    public void FadeLookUpObjects()
    {
        if (lookUpComplete)
            return;

        foreach (GameObject uiObject in lookUpObjects)
        {
            Fade(uiObject, "lookup");
        }

        if (lookUpObjects[0].GetComponent<Image>().color.a <= 0)
            lookUpComplete = true;
    }

    public IEnumerator WaitRoutine()
    {
        yield return new WaitForSeconds(1f);
        ShowControls(true);
    }

    public void TurnOffOverlay()
    {
        foreach (UIMover mover in uiMovers)
        {
            mover.gameObject.SetActive(false);
        }

        foreach (UIFader fader in uiFaders)
        {
            fader.gameObject.SetActive(false);
        }
    }

    public void UIMove(string uiMoveName)
    {
        for (int i = 0; i < uiMovers.Count; i++)
        {
            if (uiMoveName == uiMovers[i].uiMoveName)
            {
                uiMovers[i].gameObject.SetActive(true);
                
                uiMovers[i].Move();
                return;
            }
        }
    }

    public void UIFade(bool fadeOn, string uiFadeName)
    {
        for (int i = 0; i < uiFaders.Count; i++)
        {
            if (uiFadeName == uiFaders[i].uiFadeName)
            {
                uiFaders[i].gameObject.SetActive(true);
                if (fadeOn)
                {
                    uiFaders[i].FadeOn();
                }
                else
                {
                    uiFaders[i].FadeOff();
                }
                
                return;
            }
        }
    }

    public void UIUsageFade(string name)
    {
        for (int i = 0; i < uiMovers.Count; i++)
        {
            if (uiMovers[i].uiMoveName == name)
            {
                Fade(uiMovers[i].gameObject, name);
            }
        }

        for (int i = 0; i < uiFaders.Count; i++)
        {
            if (uiFaders[i].uiFadeName == name)
            {
                Fade(uiFaders[i].gameObject, name);
            }
        }


    }

    float FadeAmount(string name)
    {
        float fadeAmount = 0;

        switch (name)
        {
            case "spacebar":
                Debug.Log("SpacebarFade");
                fadeAmount = 0.3f;
                break;
            case "camera":
                Debug.Log("camera");
                fadeAmount = 0.1f;
                break;

            case "lookup":
                Debug.Log("lookup");
                fadeAmount = 0.2f;
                break;
            default:
                Debug.Log("default");
                fadeAmount = 0.01f;
                break;
        }

        return fadeAmount;
    }

    void Fade(GameObject gameObject, string name)
    {

        if (gameObject.GetComponent<Image>() != null)
        {
            Image imageObject = gameObject.GetComponent<Image>();
            imageObject.color = new Color(
                imageObject.color.r, 
                imageObject.color.g, 
                imageObject.color.b, 
                imageObject.color.a - FadeAmount(name)
                );
        }
        else if (gameObject.GetComponent<TMP_Text>() != null)
        {
            TMP_Text txtObject = gameObject.GetComponent<TMP_Text>();
            txtObject.color = new Color(
                txtObject.color.r,
                txtObject.color.g,
                txtObject.color.b,
                txtObject.color.a - FadeAmount(name)
                );
        }
        else
        {
            Debug.Log("NOTHING TO FADE");
        }
    }

    public void ShowControls(bool isShowing)
    {
        MouseLookUI(isShowing);
        WASDButtonsUI(isShowing);

    }

    public void MouseLookUI(bool isShowing)
    {
        mouslookBtn.SetActive(isShowing);

        if (isShowing)
        {
            //mouslookBtn.GetComponent<UIMover>().Move();
            mouselookImage.color = new Color(mouselookImage.color.r, mouselookImage.color.g, mouselookImage.color.b, 1);
            mouselookText.color = new Color(mouselookText.color.r, mouselookText.color.g, mouselookText.color.b, 1);
        }   
    }

    public void MouseLookUIVisibility()
    {
        if (mouselookText.color.a > 0)
            mouselookText.color = new Color(mouselookText.color.r, mouselookText.color.g, mouselookText.color.b, mouselookText.color.a - 0.01f);
        if (mouselookImage.color.a > 0)
            mouselookImage.color = new Color(mouselookImage.color.r, mouselookImage.color.g, mouselookImage.color.b, mouselookImage.color.a - 0.01f);
    }

    public void WASDButtonsUI(bool isShowing)
    {
        foreach (GameObject button in wasdButtons)
        {
            button.SetActive(isShowing);
            if (isShowing)
            {
                button.GetComponent<UIMover>().Move();

                Image btnImage = button.GetComponent<Image>();
                List<TMP_Text> wasdTexts = new List<TMP_Text>();
                wasdTexts.AddRange(button.GetComponentsInChildren<TMP_Text>());

                btnImage.color = new Color(btnImage.color.r, btnImage.color.g, btnImage.color.b, 1);

                foreach (TMP_Text text in wasdTexts)
                {
                    text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
                }
            }
                
        }
    }

    public void FindWASD(string name)
    {
        for (int i = 0; i < wasdButtons.Count; i++)
        {
            if (name == wasdButtons[i].name)
            {
                if (wasdButtons[i].GetComponent<Image>().color.a == 0)
                    return;

                WASDVisability(wasdButtons[i]);
                return;
            }
        }
    }

    public void WASDVisability(GameObject wasdButton)
    {
        Image btnImage = wasdButton.GetComponent<Image>();
        List<TMP_Text> wasdTexts = new List<TMP_Text>();
        wasdTexts.AddRange(wasdButton.GetComponentsInChildren<TMP_Text>());
        
        if (wasdButton.name == "Spacebar")
            alphaAmount = 0.3f;
        else
            alphaAmount = 0.01f;

        if (btnImage.color.a > 0)
            btnImage.color = new Color(btnImage.color.r, btnImage.color.g, btnImage.color.b, btnImage.color.a - alphaAmount);

        foreach (TMP_Text text in wasdTexts)
        {
            if (text.color.a > 0)
                text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - alphaAmount);
        }
    }

    public void EnterShipUI(bool isShowing)
    {
        enterShipBtn.SetActive(isShowing);
    }

    public void ExitShipUI(bool isShowing)
    {
        exitShipBtn.SetActive(isShowing);
    }

    public void FlagButtonUI(bool isShowing)
    {
        flagBtn.SetActive(isShowing);
        if (isShowing)
            flagBtn.GetComponent<UIMover>().Move();
    }

    public void CameraButtonUI(bool isShowing)
    {
        cameraBtn.SetActive(isShowing);
        if (isShowing)
        {
            cameraBtn.GetComponent<UIMover>().Move();
            cameraBtnImage.color = new Color(cameraBtnImage.color.r, cameraBtnImage.color.g, cameraBtnImage.color.b, 1);
            cameraText.color = new Color(cameraText.color.r, cameraText.color.g, cameraText.color.b, 1);
        }

    }

    public void ConstellationButtonUI(bool isShowing)
    {
        constellationBtn.SetActive(isShowing);
        if (isShowing)
        {
            constellationBtnImage.color = new Color(constellationBtnImage.color.r, constellationBtnImage.color.g, constellationBtnImage.color.b, 1);
            constellationText.color = new Color(constellationText.color.r, constellationText.color.g, constellationText.color.b, 1);
        }

    }

    public void ReduceCameraButtonVisability()
    {
        if(cameraText.color.a > 0)
            cameraText.color = new Color(cameraText.color.r, cameraText.color.g, cameraText.color.b, cameraText.color.a - 0.1f);
        if (cameraBtnImage.color.a > 0)
            cameraBtnImage.color = new Color(cameraBtnImage.color.r, cameraBtnImage.color.g, cameraBtnImage.color.b, cameraBtnImage.color.a - 0.1f);
    }

    public void ReduceConstellationButtonVisability()
    {
        if (constellationBtn.activeSelf)
        {
            if (constellationText.color.a > 0)
                constellationText.color = new Color(constellationText.color.r, constellationText.color.g, constellationText.color.b, constellationText.color.a - 0.1f);
            if (constellationBtnImage.color.a > 0)
                constellationBtnImage.color = new Color(constellationBtnImage.color.r, constellationBtnImage.color.g, constellationBtnImage.color.b, constellationBtnImage.color.a - 0.1f);
        }

    }
}
