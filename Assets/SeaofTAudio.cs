﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaofTAudio : MonoBehaviour
{
    [System.Serializable]
    public class AudioRecordings
    {
        public string constellationName;
        public List<AudioClip> recordings = new List<AudioClip>();
    }

    public List<AudioRecordings> audioRecordings = new List<AudioRecordings>();
}
