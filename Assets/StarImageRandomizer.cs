﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarImageRandomizer : MonoBehaviour
{
    public List<Sprite> starSprites = new List<Sprite>();

    public List<Color> starColours = new List<Color>();

    SpriteRenderer starSprite;

    void Awake()
    {
        starSprite = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        ChooseStar();
    }

    void ChooseStar()
    {
        int randomStar = Random.Range(0, starSprites.Count);

        int randomColour = Random.Range(0, starColours.Count);

        starSprite.sprite = starSprites[randomStar];
        starSprite.color = starColours[randomColour];
    }

    
}
