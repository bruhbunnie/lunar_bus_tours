﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TextInput_i : MonoBehaviour, IuiActivate
{
    public Color highlightColour;

    public Color defaultColour;

    public Collider buttonCollider;

    public TMP_InputField textInput;

    public Image highlightImage;

    public bool isClicked;

    void Awake()
    {
        buttonCollider = GetComponent<Collider>();
    }

    void Start()
    {
        highlightImage.color = defaultColour;
    }

    public void SetToActive()
    {

    }

    public void OnActivated()
    {
        StartInput();
    }

    private void StartInput()
    {
        textInput.caretWidth = 0;
        textInput.ActivateInputField();
    }

    public void OnHighlight()
    {
        highlightImage.color = highlightColour;
    }

    public bool IsLit()
    {
        if (isClicked)
            return true;
        else
            return false;
    }


    public void OnDefault()
    {
        if (!isClicked)
            highlightImage.color = defaultColour;
    }
}
