﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPageBtns : MonoBehaviour, IuiActivate
{
    public Image buttonImage;

    public TMP_Text textLabel;

    public Color highlightColour;
    public Color defaultColour;

    public Collider buttonCollider;

    public bool isPrev = false;

    public bool isBlinking = false;


    void Start()
    {
        buttonImage.color = defaultColour;
        buttonCollider = GetComponent<Collider>();

        buttonImage = GetComponent<Image>();

    }

    public void SetToActive()
    {

    }

    public void OnActivated()
    {
        ChangePage();

        if (isBlinking)
        {
            isBlinking = false;
        }
        
    }


    public void ChangePage()
    {
        int currentIndex = TutorialPagesManager._instance.currentPageIndex;
        int maxIndex = TutorialPagesManager._instance.pageCount - 1;

        if (isPrev)
        {
            if (currentIndex != 0)
                TutorialPagesManager._instance.PageChange(currentIndex - 1);
        }
        else
        {
            if (currentIndex != maxIndex)
                TutorialPagesManager._instance.PageChange(currentIndex + 1);
            else
            {
                OnDefault();
                ConsoleManager._instance.ActivateMenu("badge");
            }
                

        }
    }

    public void OnHighlight()
    {
        if (buttonImage.color == defaultColour)
        {
            buttonImage.color = highlightColour;
            textLabel.color = highlightColour;
        }
    }

    public bool IsLit()
    {

        if (buttonImage.color == highlightColour)
            return true;
        else
            return false;
    }


    public void OnDefault()
    {

        buttonImage.color = defaultColour;
        textLabel.color = defaultColour;
    }
}

