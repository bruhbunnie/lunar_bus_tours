﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPagesManager : MonoBehaviour
{
    public static TutorialPagesManager _instance;

    public List<GameObject> pages = new List<GameObject>();

    public List<GameObject> graphics = new List<GameObject>();

    public GameObject prevBtn;

    public int pageCount;

    public int currentPageIndex = 0;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        if (_instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        PageChange(0);

        pageCount = pages.Count;
    }

    public void PageChange(int pageIndex)
    {
        currentPageIndex = pageIndex;

        CloseAllPages();

        if(pageIndex == 0)
        {
            graphics[0].SetActive(true);
        }
        else
        {
            graphics[1].SetActive(true);
            graphics[2].SetActive(true);
        }

        pages[pageIndex].SetActive(true);

        switch (pageIndex)
        {
            case 0:
                ConsoleManager._instance.HideSidePanels();
                ConsoleManager._instance.TurnOffSideImages();
                break;
            case 1:
                ConsoleManager._instance.ShowSidePanels();
                ConsoleManager._instance.ImagePicker("image", 0);
                break;
            case 2:
                ConsoleManager._instance.ShowSidePanels();
                ConsoleManager._instance.ImagePicker("image", 1);
                break;
            case 3:
                ConsoleManager._instance.ShowSidePanels();
                ConsoleManager._instance.ImagePicker("image", 2);
                break;
            case 4:
                ConsoleManager._instance.HideSidePanels();
                ConsoleManager._instance.TurnOffSideImages();
                break;
        }
    }

    void CloseAllPages()
    {
        foreach (GameObject page in pages)
        {
            page.SetActive(false);
        }

        foreach (GameObject graphic in graphics)
        {
            graphic.SetActive(false);
        }
    }
}
