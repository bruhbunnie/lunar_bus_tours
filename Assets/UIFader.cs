﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIFader : MonoBehaviour
{
    // solid color (alpha is 1)
    public Color solidColor = Color.white;

    public string uiFadeName;

    // clear color (alpha is 0)
    public Color clearColor = new Color(1f, 1f, 1f, 0f);

    // delay before iTweening
    public float delay = 0.5f;

    // time for iTween animation
    public float timeToFade = 2f;

    // ease in-out for iTween animation
    public iTween.EaseType easeType = iTween.EaseType.easeOutExpo;

    // reference to Maskable Graphic component
    public Image graphic;

    public TMP_Text text;

    private void Awake()
    {
        if (GetComponent<Image>() != null)
        {
            graphic = GetComponent<Image>();
        }
        else if (GetComponent<TMP_Text>() != null)
        {
            text = GetComponent<TMP_Text>();
        }
        else
            Debug.Log("THERE IS NOTHING HERE TO CHANGE");
    }

    // use this Update the color during the iTween.ValueTo
    void UpdateColor(Color newColor)
    {
        if (graphic != null)
            graphic.color = newColor;
        else if (text != null)
            text.color = newColor;
        else
            Debug.Log("THERE IS NOTHING HERE TO CHANGE");
    }

    // use the iTween.ValueTo method to transition from solid color to clear color
    public void FadeOff()
    {
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", solidColor,
            "to", clearColor,
            "time", timeToFade,
            "delay", delay,
            "easetype", easeType,
            "onupdatetarget", gameObject,
            "onupdate", "UpdateColor"
        ));
    }

    // use the iTween.ValueTo method to transition from clear color to solid color
    public void FadeOn()
    {
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", clearColor,
            "to", solidColor,
            "time", timeToFade,
            "delay", delay,
            "easetype", easeType,
            "onupdatetarget", gameObject,
            "onupdate", "UpdateColor"
        ));
    }
}
